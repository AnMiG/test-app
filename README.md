# Architecture #
Used MVP pattern. As i am new to programming such apps - tried to combine experience from the articles and sites which i received last week.
There are four base entities: AudioBooks, Movies, Podcasts, Favorites. Each entity contains "details" and "list". When the data received from url in model layer - it is converted by presenter to view object. Then this view object is sent to view layer, and also converted to json and sent back to the model layer, but stored in SQLite databse. Like cache in offline.

Each entity has its own:

1. Contract (interfece for presenter and interface for the view).
2. Presenter (implements Contract.Presenter).
3. Fragment (implements Contract.View).
4. DataObject (received from net).
5. ViewObject (converted from DataObject/JSON when received from URL/SQL).
6. Mapper (to conver from DataObject to ViewObject).

![1399511839-Schema.png](https://bitbucket.org/repo/x8X7okA/images/1579265298-1399511839-Schema.png)

# Navigation #
As described in the test task:
 "If you select another catego](https://bitbucket.org/repo/x8X7okA/images/317447768-1399511839-Schema.png)ry by tapping on the BottomNavigationView, the state of the current view should be stored. E.g. If i open AudioBooks category, select an item, switch to Podcasts and press the back navigation button, the app should navigate back to the previously selected item from the AudioBooks category.
"
It makes confused. Because:
1) if the user wants to close app by pressing back button - he must press 100500 times to return to the first opened fragment. To avoid this have set the limit of stored states to 5.
2) Google does not recommend to restore subwindows after pressing the BotomNavigationView. It is more habitually to see the main window after pressing category button.
Thats why the navigation in my app may seem not intuitively and inconvenient

# To test the app: #
[Link to .apk file](https://drive.google.com/open?id=0B4FxehC5JH8aSUFiLVBrS3NQdDA)