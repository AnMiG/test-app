package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;

import java.util.List;

public interface IMoviesListContract {

    interface Presenter {
        void getList();

        void onItemSelected(int index);

        void addToFavorites(MoviesListVO item);

        void removeFromFavorites(MoviesListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showList(List<MoviesListVO> list);

        void showError(String message);

        void showDetailsFragment(int index);

        void showAddedToFavorites();

        void showRemovedFromFavorites();
    }

    interface DataReceiveCallback {
        void onListReceived(List<MoviesListVO> list);

        void onListReceiveError();
    }
}
