package com.example.testapp.presenter.presenters;

import android.content.Context;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.IRepository;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IMovieDetailsContract;
import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.google.gson.Gson;

import javax.inject.Inject;

/**
 * Created by anmig on 31.05.2017.
 */

public class MovieDetailsPresenter implements IMovieDetailsContract.Presenter, IMovieDetailsContract.DataReceiveCallback, IAddRemoveToFavoritesCallback {
    @Inject
    protected Context mContext;

    @Inject
    protected IRepository mRepository;

    private IMovieDetailsContract.View mView;

    public MovieDetailsPresenter(IMovieDetailsContract.View pView) {
        App.getAppComponent().inject(this);
        this.mView = pView;
    }

    @Override
    public void getDetails(String id) {
        this.mView.showLoading(true);
        this.mRepository.getMovieDetails(id, this);
    }

    @Override
    public void addToFavorites(MoviesListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(true);
        this.mRepository.addMoviesToFavorites(item.getId(), new Gson().toJson(item), this);
    }

    @Override
    public void removeFromFavorites(MoviesListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(false);
        this.mRepository.removeFromFavorites(item.getId(), this);
    }

    @Override
    public void onAddedToFavorites() {
        this.mView.showLoading(false);
        this.mView.showAddedToFavorites();
    }

    @Override
    public void onAddToFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_add_favorites));
    }

    @Override
    public void onRemovedFromFavorites() {
        this.mView.showLoading(false);
        this.mView.showRemovedFromFavorites();
    }

    @Override
    public void onRemoveFromFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_remove_favorites));
    }

    @Override
    public void onDetailsReceived(MovieDetailsVO details) {
        this.mView.showLoading(false);
        this.mView.showDetails((MovieDetailsVO) details);
    }

    @Override
    public void onDatailsReceiveError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_movie_details));
    }

    @Override
    public void onStop() {
        this.mRepository.onStop();
    }
}
