package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;

import java.util.List;

public interface IAudioBooksListContract {

    interface Presenter {
        void getList();

        void onItemSelected(int index);

        void addToFavorites(AudioBooksListVO item);

        void removeFromFavorites(AudioBooksListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showList(List<AudioBooksListVO> list);

        void showError(String message);

        void showDetailsFragment(int index);

        void showAddedToFavorites();

        void showRemovedFromFavorites();
    }

    interface DataReceiveCallback {
        void onListReceived(List<AudioBooksListVO> list);

        void onListReceiveError();
    }
}
