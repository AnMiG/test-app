package com.example.testapp.presenter.view_objects;

import com.example.testapp.presenter.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by anmig on 02.06.2017.
 */

public abstract class ViewObject implements Serializable {
    @SerializedName("addedToFav")
    @Expose
    private boolean mAddedToFavorites;

    @SerializedName("id")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAddedToFavorites() {
        return mAddedToFavorites;
    }

    public void setAddedToFavorites(boolean addedToFavorites) {
        this.mAddedToFavorites = addedToFavorites;
    }
}
