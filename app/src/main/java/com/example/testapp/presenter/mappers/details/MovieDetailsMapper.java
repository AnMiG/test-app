package com.example.testapp.presenter.mappers.details;

import com.example.testapp.model.net.data_objects.details.movie_details.MovieDetailsDO;
import com.example.testapp.model.net.data_objects.details.movie_details.Result;
import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;

import io.reactivex.functions.Function;

/**
 * Converts the dewtailed data of some item received from URL or SQL to more simplified view object
 */

public class MovieDetailsMapper implements Function<MovieDetailsDO, MovieDetailsVO> {
    @Override
    public MovieDetailsVO apply(@io.reactivex.annotations.NonNull MovieDetailsDO movieDetailsDO) throws Exception {
        if (movieDetailsDO.getResults().size() == 0)
            return null;
        Result result = movieDetailsDO.getResults().get(0);
        MovieDetailsVO vo = new MovieDetailsVO();

        vo.setId(result.getTrackId().toString());
        vo.setArtist(result.getArtistName());
        vo.setImage(result.getArtworkUrl100());
        vo.setImageWidth(100);
        vo.setImageHeight(100);
        vo.setName(result.getTrackName());
        vo.setPrice(result.getCollectionPrice());
        vo.setCurrency(result.getCurrency());
        vo.setGenre(result.getPrimaryGenreName());
        return vo;
    }
}
