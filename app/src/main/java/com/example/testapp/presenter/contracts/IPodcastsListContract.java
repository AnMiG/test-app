package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;
import com.example.testapp.presenter.view_objects.details.PodcastDetailsVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

import java.util.List;

public interface IPodcastsListContract {

    interface Presenter {
        void getList();

        void onItemSelected(int index);

        void addToFavorites(PodcastsListVO item);

        void removeFromFavorites(PodcastsListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showList(List<PodcastsListVO> list);

        void showError(String message);

        void showDetailsFragment(int index);

        void showAddedToFavorites();

        void showRemovedFromFavorites();
    }

    interface DataReceiveCallback {
        void onListReceived(List<PodcastsListVO> list);

        void onListReceiveError();
    }
}
