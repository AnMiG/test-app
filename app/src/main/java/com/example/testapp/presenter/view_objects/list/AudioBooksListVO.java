package com.example.testapp.presenter.view_objects.list;

import com.example.testapp.model.net.data_objects.list.audio_books_list.Entry;
import com.example.testapp.presenter.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AudioBooksListVO extends ListViewObject {
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("artist")
    @Expose
    private String artist;

    @SerializedName("title")
    @Expose
    private String title;

    public AudioBooksListVO() {
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}