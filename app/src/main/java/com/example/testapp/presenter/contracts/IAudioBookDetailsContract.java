package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.view_objects.details.AudioBookDetailsVO;
import com.example.testapp.presenter.view_objects.details.DetailsViewObject;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;

import java.util.List;

public interface IAudioBookDetailsContract {

    interface Presenter {
        void getDetails(String id);

        void addToFavorites(AudioBooksListVO item);

        void removeFromFavorites(AudioBooksListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showDetails(AudioBookDetailsVO details);

        void showAddedToFavorites();

        void showRemovedFromFavorites();

        void showError(String message);
    }

    interface DataReceiveCallback {
        void onDetailsReceived(AudioBookDetailsVO details);

        void onDatailsReceiveError();
    }
}
