package com.example.testapp.presenter.contracts;

/**
 * Created by anmig on 04.06.2017.
 */

public interface IAddRemoveToFavoritesCallback {
    void onAddedToFavorites();

    void onAddToFavoritesError();

    void onRemovedFromFavorites();

    void onRemoveFromFavoritesError();
}
