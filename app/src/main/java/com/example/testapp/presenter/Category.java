package com.example.testapp.presenter;

/**
 * Created by anmig on 04.06.2017.
 */

public enum Category {
    AUDIO_BOOKS, MOVIES, PODCASTS;

    public static Category parseCategory(Object o) {
        if (o instanceof com.example.testapp.model.net.data_objects.list.audio_books_list.Category) {
            return AUDIO_BOOKS;
        }
        if (o instanceof com.example.testapp.model.net.data_objects.list.movies_list.Category) {
            return MOVIES;
        }
        if (o instanceof com.example.testapp.model.net.data_objects.list.podcasts_list.Category) {
            return PODCASTS;
        }
        return null;
    }
}
