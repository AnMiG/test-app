package com.example.testapp.presenter.mappers.details;

import com.example.testapp.model.net.data_objects.details.audio_book_details.AudioBookDetailsDO;
import com.example.testapp.model.net.data_objects.details.audio_book_details.Result;
import com.example.testapp.presenter.view_objects.details.AudioBookDetailsVO;

import io.reactivex.functions.Function;

/**
 * Converts the dewtailed data of some item received from URL or SQL to more simplified view object
 */

public class AudioBookDetailsMapper implements Function<AudioBookDetailsDO, AudioBookDetailsVO> {
    @Override
    public AudioBookDetailsVO apply(@io.reactivex.annotations.NonNull AudioBookDetailsDO audioBookDetailsDO) throws Exception {
        if (audioBookDetailsDO.getResults().size() == 0)
            return null;
        Result result = audioBookDetailsDO.getResults().get(0);
        AudioBookDetailsVO vo = new AudioBookDetailsVO();

        vo.setId(result.getCollectionId().toString());
        vo.setArtist(result.getArtistName());
        vo.setImage(result.getArtworkUrl100());
        vo.setImageWidth(100);
        vo.setImageHeight(100);
        vo.setName(result.getCollectionName());
        vo.setPrice(result.getCollectionPrice());
        vo.setCurrency(result.getCurrency());
        vo.setGenre(result.getPrimaryGenreName());
        return vo;
    }
}
