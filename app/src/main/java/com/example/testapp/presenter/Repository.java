package com.example.testapp.presenter;

import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.dependedncy_injection.Constants;
import com.example.testapp.model.IModel;
import com.example.testapp.model.Model;
import com.example.testapp.model.net.IApiInterface;
import com.example.testapp.model.net.data_objects.details.audio_book_details.AudioBookDetailsDO;
import com.example.testapp.model.net.data_objects.details.movie_details.MovieDetailsDO;
import com.example.testapp.model.net.data_objects.details.podcast_details.PodcastDetailsDO;
import com.example.testapp.model.net.data_objects.list.audio_books_list.AudioBooksListDO;
import com.example.testapp.model.net.data_objects.list.movies_list.MoviesListDO;
import com.example.testapp.model.net.data_objects.list.podcasts_list.PodcastsListDO;
import com.example.testapp.model.sql.ISqLiteInterface;
import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;
import com.example.testapp.presenter.contracts.IAudioBooksListContract;
import com.example.testapp.presenter.contracts.IFavoritesListContract;
import com.example.testapp.presenter.contracts.IMovieDetailsContract;
import com.example.testapp.presenter.contracts.IMoviesListContract;
import com.example.testapp.presenter.contracts.IPodcastDetailsContract;
import com.example.testapp.presenter.contracts.IPodcastsListContract;
import com.example.testapp.presenter.mappers.details.AudioBookDetailsMapper;
import com.example.testapp.presenter.mappers.details.MovieDetailsMapper;
import com.example.testapp.presenter.mappers.details.PodcastDetailsMapper;
import com.example.testapp.presenter.mappers.list.AudioBooksListMapper;
import com.example.testapp.presenter.mappers.list.MoviesListMapper;
import com.example.testapp.presenter.mappers.list.PodcastsListMapper;
import com.example.testapp.presenter.view_objects.details.AudioBookDetailsVO;
import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;
import com.example.testapp.presenter.view_objects.details.PodcastDetailsVO;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class Repository implements IRepository {
    @Inject
    protected CompositeDisposable mDisposables;

    @Inject
    protected IModel mModel;

    private IApiInterface mApi;

    private ISqLiteInterface mSql;

    public Repository() {
        App.getAppComponent().inject(this);
        this.mApi = this.mModel.getApi();
        this.mSql = this.mModel.getSql();
    }

    @Override
    public void onStop() {
        this.mDisposables.clear();
    }

    @Override
    public void getAudioBooksList(final IAudioBooksListContract.DataReceiveCallback callBack) {
        Observable<AudioBooksListDO> observable = mApi.getAudioBooksData(Constants.COUNTRY_CODE, Constants.OBJECTS_LIMIT);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new AudioBooksListMapper())
                .subscribeWith(new DisposableObserver<List<AudioBooksListVO>>() {
                    @Override
                    public void onNext(@NonNull List<AudioBooksListVO> list) {
                        //save to cache
                        Map<String, String> map = new HashMap<>();
                        Gson gson = new Gson();
                        mSql.open();
                        for (int i = 0; i < list.size(); i++) {
                            AudioBooksListVO vo = list.get(i);
                            //set is in favourites or not
                            vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                            vo.setCategory(Category.parseCategory(vo.getCategory()));
                            map.put(vo.getId(), gson.toJson(vo));
                        }
                        //mSql.close(); no need as it will be closed in mSql.addAudioBooksToCache(map);
                        mSql.addAudioBooksToCache(map);
                        //notify about finishing
                        callBack.onListReceived(list);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try {
                            List<String> jsonList = mSql.getCachedAudioBooks();
                            List<AudioBooksListVO> voList = new ArrayList<>();
                            Gson gson = new Gson();
                            mSql.open();
                            for (int i = 0; i < jsonList.size(); i++) {
                                AudioBooksListVO vo = gson.fromJson(jsonList.get(i), AudioBooksListVO.class);
                                vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                vo.setCategory(Category.parseCategory(vo.getCategory()));
                                voList.add(vo);
                            }
                            mSql.close();
                            callBack.onListReceived(voList);
                        } catch (Exception e1) {
                            callBack.onListReceiveError();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        this.mDisposables.add(disposable);
    }

    @Override
    public void getMoviesList(final IMoviesListContract.DataReceiveCallback callBack) {
        Observable<MoviesListDO> observable = mApi.getMoviesData(Constants.COUNTRY_CODE, Constants.OBJECTS_LIMIT);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new MoviesListMapper())
                .subscribeWith(new DisposableObserver<List<MoviesListVO>>() {
                    @Override
                    public void onNext(@NonNull List<MoviesListVO> list) {
                        //save to cache
                        Map<String, String> map = new HashMap<>();
                        Gson gson = new Gson();
                        mSql.open();
                        for (int i = 0; i < list.size(); i++) {
                            MoviesListVO vo = list.get(i);
                            //set is in favourites or not
                            vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                            vo.setCategory(Category.parseCategory(vo.getCategory()));
                            map.put(vo.getId(), gson.toJson(vo));
                        }
                        //mSql.close(); no need as it will be closed in mSql.addAudioBooksToCache(map);
                        mSql.addMoviesToCache(map);
                        //notify about finishing
                        callBack.onListReceived(list);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try {
                            List<String> jsonList = mSql.getCachedMovies();
                            List<MoviesListVO> voList = new ArrayList<>();
                            Gson gson = new Gson();
                            mSql.open();
                            for (int i = 0; i < jsonList.size(); i++) {
                                MoviesListVO vo = gson.fromJson(jsonList.get(i), MoviesListVO.class);
                                vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                vo.setCategory(Category.parseCategory(vo.getCategory()));
                                voList.add(vo);
                            }
                            mSql.close();
                            callBack.onListReceived(voList);
                        } catch (Exception e1) {
                            callBack.onListReceiveError();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        this.mDisposables.add(disposable);
    }

    @Override
    public void getPodcastsList(final IPodcastsListContract.DataReceiveCallback callBack) {
        Observable<PodcastsListDO> observable = mApi.getPodcastsData(Constants.COUNTRY_CODE, Constants.OBJECTS_LIMIT);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new PodcastsListMapper())
                .subscribeWith(new DisposableObserver<List<PodcastsListVO>>() {
                    @Override
                    public void onNext(@NonNull List<PodcastsListVO> list) {
                        //save to cache
                        Map<String, String> map = new HashMap<>();
                        Gson gson = new Gson();
                        mSql.open();
                        for (int i = 0; i < list.size(); i++) {
                            PodcastsListVO vo = list.get(i);
                            //set is in favourites or not
                            vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                            vo.setCategory(Category.parseCategory(vo.getCategory()));
                            map.put(vo.getId(), gson.toJson(vo));
                        }
                        //mSql.close(); no need as it will be closed in mSql.addAudioBooksToCache(map);
                        mSql.addPodcastsToCache(map);
                        //notify about finishing
                        callBack.onListReceived(list);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        try {
                            List<String> jsonList = mSql.getCachedPodcasts();
                            List<PodcastsListVO> voList = new ArrayList<>();
                            Gson gson = new Gson();
                            mSql.open();
                            for (int i = 0; i < jsonList.size(); i++) {
                                PodcastsListVO vo = gson.fromJson(jsonList.get(i), PodcastsListVO.class);
                                vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                vo.setCategory(Category.parseCategory(vo.getCategory()));
                                voList.add(vo);
                            }
                            mSql.close();
                            callBack.onListReceived(voList);
                        } catch (Exception e1) {
                            callBack.onListReceiveError();
                        }
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        this.mDisposables.add(disposable);
    }

    @Override
    public void getFavouritesList(final IFavoritesListContract.DataReceiveCallback callback) {
        try {
            mSql.open();
            List<AudioBooksListVO> audioBooks = getFavoriteAudioBooks();
            List<MoviesListVO> movies = getFavoriteMovies();
            List<PodcastsListVO> podcasts = getFavoritePodcasts();
            mSql.close();
            callback.onListReceived(audioBooks, movies, podcasts);
        } catch (Exception e) {
            e.printStackTrace();
            callback.onListReceiveError();
        }
    }

    private List<AudioBooksListVO> getFavoriteAudioBooks() throws Exception {
        List<String> jsonList = mSql.getFavoriteAudioBooks();
        List<AudioBooksListVO> voList = new ArrayList<>();
        Gson gson = new Gson();
        if (jsonList != null) {
            for (int i = 0; i < jsonList.size(); i++) {
                AudioBooksListVO vo = gson.fromJson(jsonList.get(i), AudioBooksListVO.class);
                vo.setAddedToFavorites(true);
                vo.setCategory(Category.AUDIO_BOOKS);
                voList.add(vo);
            }
        }
        return voList;
    }

    private List<MoviesListVO> getFavoriteMovies() throws Exception {
        List<String> jsonList = mSql.getFavoriteMovies();
        List<MoviesListVO> voList = new ArrayList<>();
        Gson gson = new Gson();
        if (jsonList != null) {
            for (int i = 0; i < jsonList.size(); i++) {
                MoviesListVO vo = gson.fromJson(jsonList.get(i), MoviesListVO.class);
                vo.setAddedToFavorites(true);
                vo.setCategory(Category.MOVIES);
                voList.add(vo);
            }
        }
        return voList;
    }

    private List<PodcastsListVO> getFavoritePodcasts() throws Exception {
        List<String> jsonList = mSql.getFavoritePodcasts();
        List<PodcastsListVO> voList = new ArrayList<>();
        Gson gson = new Gson();
        if (jsonList != null) {
            for (int i = 0; i < jsonList.size(); i++) {
                PodcastsListVO vo = gson.fromJson(jsonList.get(i), PodcastsListVO.class);
                vo.setAddedToFavorites(true);
                vo.setCategory(Category.PODCASTS);
                voList.add(vo);
            }
        }
        return voList;
    }

    @Override
    public void getAudioBookDetails(final String id, final IAudioBookDetailsContract.DataReceiveCallback callBack) {
        Observable<AudioBookDetailsDO> observable = mApi.getAudioBooksDetailsData(id);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new AudioBookDetailsMapper())
                .subscribeWith(new DisposableObserver<AudioBookDetailsVO>() {
                                   @Override
                                   public void onNext(@NonNull AudioBookDetailsVO details) {
                                       mSql.open();
                                       details.setAddedToFavorites(mSql.isAddedToFavorites(details.getId()));
                                       mSql.addAudioBookDetailsToCache(details.getId(), new Gson().toJson(details));
                                       mSql.close();
                                       callBack.onDetailsReceived(details);
                                   }

                                   @Override
                                   public void onError(@NonNull Throwable e) {
                                       try {
                                           mSql.open();
                                           Gson gson = new Gson();
                                           AudioBookDetailsVO vo = gson.fromJson(mSql.getCachedAudioBookDetails(id), AudioBookDetailsVO.class);
                                           vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                           mSql.close();
                                           callBack.onDetailsReceived(vo);
                                           return;
                                       } catch (Exception e1) {
                                           callBack.onDatailsReceiveError();
                                           return;
                                       }
                                   }

                                   @Override
                                   public void onComplete() {

                                   }
                               }
                );
        this.mDisposables.add(disposable);
    }

    @Override
    public void getMovieDetails(final String id, final IMovieDetailsContract.DataReceiveCallback callBack) {
        Observable<MovieDetailsDO> observable = mApi.getMoviesDetailsData(id);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new MovieDetailsMapper())
                .subscribeWith(new DisposableObserver<MovieDetailsVO>() {
                                   @Override
                                   public void onNext(@NonNull MovieDetailsVO details) {
                                       mSql.open();
                                       details.setAddedToFavorites(mSql.isAddedToFavorites(details.getId()));
                                       mSql.addMovieDetailsToCache(details.getId(), new Gson().toJson(details));
                                       mSql.close();
                                       callBack.onDetailsReceived(details);
                                   }

                                   @Override
                                   public void onError(@NonNull Throwable e) {
                                       try {
                                           mSql.open();
                                           Gson gson = new Gson();
                                           MovieDetailsVO vo = gson.fromJson(mSql.getCachedMovieDetails(id), MovieDetailsVO.class);
                                           vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                           mSql.close();
                                           callBack.onDetailsReceived(vo);
                                           return;
                                       } catch (Exception e1) {
                                           callBack.onDatailsReceiveError();
                                       }
                                   }

                                   @Override
                                   public void onComplete() {

                                   }
                               }
                );
        this.mDisposables.add(disposable);
    }

    @Override
    public void getPodcastDetails(final String id, final IPodcastDetailsContract.DataReceiveCallback callBack) {
        Observable<PodcastDetailsDO> observable = mApi.getPodcastsDetailsData(id);
        Disposable disposable = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new PodcastDetailsMapper())
                .subscribeWith(new DisposableObserver<PodcastDetailsVO>() {
                                   @Override
                                   public void onNext(@NonNull PodcastDetailsVO details) {
                                       mSql.open();
                                       details.setAddedToFavorites(mSql.isAddedToFavorites(details.getId()));
                                       mSql.addPodcastDetailsToCache(details.getId(), new Gson().toJson(details));
                                       mSql.close();
                                       callBack.onDetailsReceived(details);
                                   }

                                   @Override
                                   public void onError(@NonNull Throwable e) {
                                       try {
                                           mSql.open();
                                           Gson gson = new Gson();
                                           PodcastDetailsVO vo = gson.fromJson(mSql.getCachedPodcastDetails(id), PodcastDetailsVO.class);
                                           vo.setAddedToFavorites(mSql.isAddedToFavorites(vo.getId()));
                                           mSql.close();
                                           callBack.onDetailsReceived(vo);
                                           return;
                                       } catch (Exception e1) {
                                           callBack.onDatailsReceiveError();
                                       }
                                   }

                                   @Override
                                   public void onComplete() {

                                   }
                               }
                );
        this.mDisposables.add(disposable);
    }

    @Override
    public void addAudioBooksToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack) {
        try {
            this.mSql.addAudioBooksToFavorites(id, json);
        } catch (Exception e) {
            callBack.onAddToFavoritesError();
            return;
        }
        callBack.onAddedToFavorites();
    }

    @Override
    public void addMoviesToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack) {
        try {
            this.mSql.addMoviesToFavorites(id, json);
        } catch (Exception e) {
            callBack.onAddToFavoritesError();
            return;
        }
        callBack.onAddedToFavorites();
    }

    @Override
    public void addPodcastsToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack) {
        try {
            this.mSql.addPodcastsToFavorites(id, json);
        } catch (Exception e) {
            callBack.onAddToFavoritesError();
            return;
        }
        callBack.onAddedToFavorites();
    }

    @Override
    public void removeFromFavorites(String id, IAddRemoveToFavoritesCallback callBack) {
        try {
            this.mSql.removeFromFavorites(id);
        } catch (Exception e) {
            callBack.onRemoveFromFavoritesError();
            return;
        }
        callBack.onRemovedFromFavorites();
    }
}
