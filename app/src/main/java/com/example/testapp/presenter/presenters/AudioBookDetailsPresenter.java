package com.example.testapp.presenter.presenters;

import android.content.Context;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.dependedncy_injection.FragmentScope;
import com.example.testapp.presenter.IRepository;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;
import com.example.testapp.presenter.view_objects.details.AudioBookDetailsVO;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.google.gson.Gson;

import javax.inject.Inject;

public class AudioBookDetailsPresenter implements IAudioBookDetailsContract.Presenter, IAudioBookDetailsContract.DataReceiveCallback, IAddRemoveToFavoritesCallback {
    @Inject
    protected Context mContext;

    @Inject
    protected IRepository mRepository;

    private IAudioBookDetailsContract.View mView;

    @Inject
    public AudioBookDetailsPresenter(IAudioBookDetailsContract.View pView) {
        this.mView = pView;
    }

    @Override
    public void getDetails(String id) {
        this.mView.showLoading(true);
        this.mRepository.getAudioBookDetails(id, this);
    }

    @Override
    public void addToFavorites(AudioBooksListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(true);
        this.mRepository.addAudioBooksToFavorites(item.getId(), new Gson().toJson(item), this);
    }

    @Override
    public void removeFromFavorites(AudioBooksListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(false);
        this.mRepository.removeFromFavorites(item.getId(), this);
    }

    @Override
    public void onAddedToFavorites() {
        this.mView.showLoading(false);
        this.mView.showAddedToFavorites();
    }

    @Override
    public void onAddToFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_add_favorites));
    }

    @Override
    public void onRemovedFromFavorites() {
        this.mView.showLoading(false);
        this.mView.showRemovedFromFavorites();
    }

    @Override
    public void onRemoveFromFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_remove_favorites));
    }

    @Override
    public void onDetailsReceived(AudioBookDetailsVO details) {
        this.mView.showLoading(false);
        this.mView.showDetails(details);
    }

    @Override
    public void onDatailsReceiveError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_audio_book_details));
    }

    @Override
    public void onStop() {
        this.mRepository.onStop();
    }
}
