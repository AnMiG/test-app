package com.example.testapp.presenter.mappers.list;

import com.example.testapp.model.net.data_objects.list.audio_books_list.Entry;
import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.model.net.data_objects.list.audio_books_list.AudioBooksListDO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Converts the data received from URL or SQL to the list of simplified view objects
 */

public class AudioBooksListMapper implements Function<AudioBooksListDO, List<AudioBooksListVO>> {
    @Override
    public List<AudioBooksListVO> apply(@io.reactivex.annotations.NonNull AudioBooksListDO audioBooksListDO) throws Exception {
        if (audioBooksListDO.getFeed().getEntry().size() == 0)
            return null;
        List<AudioBooksListVO> listObjects = new ArrayList<>();
        for (int i = 0; i < audioBooksListDO.getFeed().getEntry().size(); i++) {
            Entry entry = audioBooksListDO.getFeed().getEntry().get(i);
            AudioBooksListVO vo = new AudioBooksListVO();
            vo.setId(entry.getId().getAttributes().getImId());
            vo.setArtist(entry.getImArtist().getLabel());
            vo.setTitle(entry.getTitle().getLabel());
            if (entry.getImImage().size() > 2)
                vo.setImage(entry.getImImage().get(2).getLabel());
            vo.setCategory(Category.parseCategory(entry.getCategory()));
            listObjects.add(vo);
        }
        return listObjects;
    }
}
