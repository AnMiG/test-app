package com.example.testapp.presenter.mappers.list;

import com.example.testapp.model.net.data_objects.list.movies_list.Entry;
import com.example.testapp.model.net.data_objects.list.movies_list.MoviesListDO;
import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Converts the data received from URL or SQL to the list of simplified view objects
 */

public class MoviesListMapper implements Function<MoviesListDO, List<MoviesListVO>> {
    @Override
    public List<MoviesListVO> apply(@io.reactivex.annotations.NonNull MoviesListDO moviesListDO) throws Exception {
        if (moviesListDO.getFeed().getEntry().size() == 0)
            return null;
        List<MoviesListVO> listObjects = new ArrayList<>();
        for (int i = 0; i < moviesListDO.getFeed().getEntry().size(); i++) {
            Entry entry = moviesListDO.getFeed().getEntry().get(i);
            MoviesListVO vo = new MoviesListVO();
            vo.setId(entry.getId().getAttributes().getImId());
            vo.setArtist(entry.getImArtist().getLabel());
            vo.setTitle(entry.getTitle().getLabel());
            if (entry.getImImage().size() >2)
                vo.setImage(entry.getImImage().get(2).getLabel());
            vo.setCategory(Category.parseCategory(entry.getCategory()));
            listObjects.add(vo);
        }
        return listObjects;
    }
}
