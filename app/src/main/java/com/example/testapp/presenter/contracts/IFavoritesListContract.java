package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

import java.util.List;

public interface IFavoritesListContract {

    interface Presenter {
        void getList();

        void onItemSelected(ListViewObject vo);

        void removeFromFavorites(ListViewObject vo);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showList(List<AudioBooksListVO> audioBooksList, List<MoviesListVO> moviesList, List<PodcastsListVO> podcastssList);

        void showError(String message);

        void showAudioBookDetailsFragment(String viewObjectJson);

        void showMoviesDetailsFragment(String viewObjectJson);

        void showPodcastDetailsFragment(String viewObjectJson);

        void showRemovedFromFavorites();
    }

    interface DataReceiveCallback {
        void onListReceived(List<AudioBooksListVO> audioBooksList, List<MoviesListVO> moviesList, List<PodcastsListVO> podcastssList);

        void onListReceiveError();
    }
}
