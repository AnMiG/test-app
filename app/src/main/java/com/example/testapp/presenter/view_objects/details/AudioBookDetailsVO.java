package com.example.testapp.presenter.view_objects.details;

import com.example.testapp.model.net.data_objects.details.audio_book_details.Result;

import java.lang.Integer;
import java.lang.String;
import java.lang.Double;

import com.example.testapp.presenter.view_objects.details.DetailsViewObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AudioBookDetailsVO extends DetailsViewObject {
    @SerializedName("artist")
    @Expose
    private String artist;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("imageWidth")
    @Expose
    private int imageWidth;

    @SerializedName("imageHeight")
    @Expose
    private int imageHeight;

    @SerializedName("price")
    @Expose
    private Double price;

    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("genre")
    @Expose
    private String genre;

    public AudioBookDetailsVO() {
    }

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}