package com.example.testapp.presenter.mappers.details;

import com.example.testapp.model.net.data_objects.details.podcast_details.PodcastDetailsDO;
import com.example.testapp.model.net.data_objects.details.podcast_details.Result;
import com.example.testapp.presenter.view_objects.details.PodcastDetailsVO;

import io.reactivex.functions.Function;

/**
 * Converts the dewtailed data of some item received from URL or SQL to more simplified view object
 */

public class PodcastDetailsMapper implements Function<PodcastDetailsDO, PodcastDetailsVO> {
    @Override
    public PodcastDetailsVO apply(@io.reactivex.annotations.NonNull PodcastDetailsDO podcastDetailsDO) throws Exception {
        if (podcastDetailsDO.getResults().size() == 0)
            return null;
        Result result = podcastDetailsDO.getResults().get(0);
        PodcastDetailsVO vo = new PodcastDetailsVO();

        vo.setId(result.getCollectionId().toString());
        vo.setArtist(result.getArtistName());
        vo.setImage(result.getArtworkUrl100());
        vo.setImageWidth(100);
        vo.setImageHeight(100);
        vo.setName(result.getCollectionName());
        vo.setPrice(result.getCollectionPrice());
        vo.setCurrency(result.getCurrency());
        vo.setGenre(result.getPrimaryGenreName());
        return vo;
    }
}
