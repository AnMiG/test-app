package com.example.testapp.presenter.presenters;

import android.content.Context;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.IRepository;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IFavoritesListContract;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anmig on 31.05.2017.
 */

public class FavoritesListPresenter implements IFavoritesListContract.Presenter, IFavoritesListContract.DataReceiveCallback, IAddRemoveToFavoritesCallback {
    @Inject
    protected Context mContext;

    @Inject
    protected IRepository mRepository;

    private IFavoritesListContract.View mView;

    public FavoritesListPresenter(IFavoritesListContract.View pView) {
        App.getAppComponent().inject(this);
        this.mView = pView;
    }

    @Override
    public void getList() {
        this.mView.showLoading(true);
        this.mRepository.getFavouritesList(this);
    }

    @Override
    public void onItemSelected(ListViewObject vo) {
        Category category = vo.getCategory();
        Gson gson = new Gson();
        switch (category) {
            case AUDIO_BOOKS:
                this.mView.showAudioBookDetailsFragment(gson.toJson(vo));
                break;
            case MOVIES:
                this.mView.showMoviesDetailsFragment(gson.toJson(vo));
                break;
            case PODCASTS:
                this.mView.showPodcastDetailsFragment(gson.toJson(vo));
                break;
        }
    }

    @Override
    public void removeFromFavorites(ListViewObject vo) {
        this.mView.showLoading(true);
        vo.setAddedToFavorites(false);
        this.mRepository.removeFromFavorites(vo.getId(), this);
    }

    @Override
    public void onListReceived(List<AudioBooksListVO> audioBooksList, List<MoviesListVO> moviesList, List<PodcastsListVO> podcastsList) {
        this.mView.showList(audioBooksList, moviesList, podcastsList);
        this.mView.showLoading(false);
    }

    @Override
    public void onListReceiveError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_favorites_list));
    }

    @Override
    public void onAddedToFavorites() {
        //Do nothing
    }

    @Override
    public void onAddToFavoritesError() {
        //Do nothing
    }

    @Override
    public void onRemovedFromFavorites() {
        this.mView.showLoading(false);
        this.mView.showRemovedFromFavorites();
    }

    @Override
    public void onRemoveFromFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_remove_favorites));
    }

    @Override
    public void onStop() {
        this.mRepository.onStop();
    }
}
