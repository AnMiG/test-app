package com.example.testapp.presenter.presenters;

import android.content.Context;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.IRepository;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IPodcastsListContract;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anmig on 31.05.2017.
 */

public class PodcastsListPresenter implements IPodcastsListContract.Presenter, IPodcastsListContract.DataReceiveCallback, IAddRemoveToFavoritesCallback {
    @Inject
    protected Context mContext;

    @Inject
    protected IRepository mRepository;

    private IPodcastsListContract.View mView;

    public PodcastsListPresenter(IPodcastsListContract.View pView) {
        App.getAppComponent().inject(this);
        this.mView = pView;
    }

    @Override
    public void getList() {
        this.mView.showLoading(true);
        this.mRepository.getPodcastsList(this);
    }

    @Override
    public void onItemSelected(int index) {
        this.mView.showDetailsFragment(index);
    }

    @Override
    public void addToFavorites(PodcastsListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(true);
        this.mRepository.addPodcastsToFavorites(item.getId(), new Gson().toJson(item), this);
    }

    @Override
    public void removeFromFavorites(PodcastsListVO item) {
        this.mView.showLoading(true);
        item.setAddedToFavorites(false);
        this.mRepository.removeFromFavorites(item.getId(), this);
    }

    @Override
    public void onListReceived(List<PodcastsListVO> list) {
        this.mView.showList(list);
        this.mView.showLoading(false);
    }

    @Override
    public void onListReceiveError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_podcasts_list));
    }

    @Override
    public void onAddedToFavorites() {
        this.mView.showLoading(false);
        this.mView.showAddedToFavorites();
    }

    @Override
    public void onAddToFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_add_favorites));
    }

    @Override
    public void onRemovedFromFavorites() {
        this.mView.showLoading(false);
        this.mView.showRemovedFromFavorites();
    }

    @Override
    public void onRemoveFromFavoritesError() {
        this.mView.showLoading(false);
        this.mView.showError(mContext.getString(R.string.error_remove_favorites));
    }

    @Override
    public void onStop() {
        this.mRepository.onStop();
    }
}
