package com.example.testapp.presenter.mappers.list;

import com.example.testapp.model.net.data_objects.list.podcasts_list.Entry;
import com.example.testapp.model.net.data_objects.list.podcasts_list.PodcastsListDO;
import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Converts the data received from URL or SQL to the list of simplified view objects
 */

public class PodcastsListMapper implements Function<PodcastsListDO, List<PodcastsListVO>> {
    @Override
    public List<PodcastsListVO> apply(@io.reactivex.annotations.NonNull PodcastsListDO podcastsListDO) throws Exception {
        if (podcastsListDO.getFeed().getEntry().size() == 0)
            return null;
        List<PodcastsListVO> listObjects = new ArrayList<>();
        for (int i = 0; i < podcastsListDO.getFeed().getEntry().size(); i++) {
            Entry entry = podcastsListDO.getFeed().getEntry().get(i);
            PodcastsListVO vo = new PodcastsListVO();
            vo.setId(entry.getId().getAttributes().getImId());
            vo.setArtist(entry.getImArtist().getLabel());
            vo.setTitle(entry.getTitle().getLabel());
            if (entry.getImImage().size() > 2)
                vo.setImage(entry.getImImage().get(2).getLabel());
            vo.setCategory(Category.parseCategory(entry.getCategory()));
            listObjects.add(vo);
        }
        return listObjects;
    }
}
