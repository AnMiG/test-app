package com.example.testapp.presenter;

import com.example.testapp.presenter.contracts.IAddRemoveToFavoritesCallback;
import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;
import com.example.testapp.presenter.contracts.IAudioBooksListContract;
import com.example.testapp.presenter.contracts.IFavoritesListContract;
import com.example.testapp.presenter.contracts.IMovieDetailsContract;
import com.example.testapp.presenter.contracts.IMoviesListContract;
import com.example.testapp.presenter.contracts.IPodcastDetailsContract;
import com.example.testapp.presenter.contracts.IPodcastsListContract;

/**
 * Created by anmig on 06.06.2017.
 */

public interface IRepository {

    void getAudioBooksList(final IAudioBooksListContract.DataReceiveCallback callBack);

    void getMoviesList(final IMoviesListContract.DataReceiveCallback callBack);

    void getPodcastsList(final IPodcastsListContract.DataReceiveCallback callBack);

    void getFavouritesList(final IFavoritesListContract.DataReceiveCallback callback);


    void getAudioBookDetails(String id, final IAudioBookDetailsContract.DataReceiveCallback callBack);

    void getMovieDetails(String id, final IMovieDetailsContract.DataReceiveCallback callBack);

    void getPodcastDetails(String id, final IPodcastDetailsContract.DataReceiveCallback callBack);


    void addAudioBooksToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack);

    void addMoviesToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack);

    void addPodcastsToFavorites(String id, String json, IAddRemoveToFavoritesCallback callBack);


    void removeFromFavorites(String id, IAddRemoveToFavoritesCallback callBack);

    void onStop();
}
