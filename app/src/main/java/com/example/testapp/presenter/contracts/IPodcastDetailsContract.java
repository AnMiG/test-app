package com.example.testapp.presenter.contracts;

import com.example.testapp.presenter.view_objects.details.PodcastDetailsVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

public interface IPodcastDetailsContract {

    interface Presenter {
        void getDetails(String id);

        void addToFavorites(PodcastsListVO item);

        void removeFromFavorites(PodcastsListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showDetails(PodcastDetailsVO details);

        void showAddedToFavorites();

        void showRemovedFromFavorites();

        void showError(String message);
    }

    interface DataReceiveCallback {
        void onDetailsReceived(PodcastDetailsVO details);

        void onDatailsReceiveError();
    }
}
