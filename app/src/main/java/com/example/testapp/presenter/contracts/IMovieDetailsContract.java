package com.example.testapp.presenter.contracts;


import com.example.testapp.presenter.view_objects.details.MovieDetailsVO;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;

public interface IMovieDetailsContract {

    interface Presenter {
        void getDetails(String id);

        void addToFavorites(MoviesListVO item);

        void removeFromFavorites(MoviesListVO item);

        void onStop();
    }

    interface View {
        void showLoading(boolean state);

        void showDetails(MovieDetailsVO details);

        void showAddedToFavorites();

        void showRemovedFromFavorites();

        void showError(String message);
    }

    interface DataReceiveCallback {
        void onDetailsReceived(MovieDetailsVO details);

        void onDatailsReceiveError();
    }
}
