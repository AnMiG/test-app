package com.example.testapp.presenter.view_objects.list;

import com.example.testapp.presenter.Category;
import com.example.testapp.presenter.view_objects.ViewObject;
import com.example.testapp.presenter.view_objects.details.DetailsViewObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anmig on 02.06.2017.
 */

public abstract class ListViewObject extends ViewObject {
    @SerializedName("category")
    @Expose
    private Category mCategory;

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category pCategory) {
        this.mCategory = pCategory;
    }
}
