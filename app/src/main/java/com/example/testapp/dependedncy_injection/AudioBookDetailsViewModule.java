package com.example.testapp.dependedncy_injection;

import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;

import dagger.Module;
import dagger.Provides;

@Module
public class AudioBookDetailsViewModule {
    private IAudioBookDetailsContract.View mView;

    public AudioBookDetailsViewModule(IAudioBookDetailsContract.View pView) {
        this.mView = pView;
    }

    @Provides
    @FragmentScope
    IAudioBookDetailsContract.View provideAudioBookDetailsView() {
        return this.mView;
    }
}
