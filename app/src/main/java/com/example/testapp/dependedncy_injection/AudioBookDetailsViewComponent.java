package com.example.testapp.dependedncy_injection;

import com.example.testapp.view.fragments.details.AudioBookDetailsFragment;
import com.example.testapp.view.fragments.list.AudioBooksListFragment;

import dagger.Subcomponent;

@FragmentScope
@Subcomponent(modules = AudioBookDetailsViewModule.class)

public interface AudioBookDetailsViewComponent {
    void inject(AudioBookDetailsFragment view);
}
