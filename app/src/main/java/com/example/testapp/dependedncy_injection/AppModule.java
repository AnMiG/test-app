package com.example.testapp.dependedncy_injection;

import android.content.Context;
import android.support.annotation.NonNull;

import com.example.testapp.view.navigation_strategy.FragmentsNavigationStrategy;
import com.example.testapp.view.navigation_strategy.IFragmentsNavigationStrategy;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private Context mContext;

    public AppModule(@NonNull Context pContext) {
        this.mContext = pContext;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return this.mContext;
    }

    @Provides
    @Singleton
    IFragmentsNavigationStrategy provideNavigationStrategy() {
        return new FragmentsNavigationStrategy();
    }
}
