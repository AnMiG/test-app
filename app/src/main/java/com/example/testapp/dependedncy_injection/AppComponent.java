package com.example.testapp.dependedncy_injection;

import com.example.testapp.model.Model;
import com.example.testapp.model.sql.SqlImpl;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.presenters.AudioBookDetailsPresenter;
import com.example.testapp.presenter.presenters.AudioBooksListPresenter;
import com.example.testapp.presenter.presenters.FavoritesListPresenter;
import com.example.testapp.presenter.presenters.MovieDetailsPresenter;
import com.example.testapp.presenter.presenters.MoviesListPresenter;
import com.example.testapp.presenter.presenters.PodcastDetailsPresenter;
import com.example.testapp.presenter.presenters.PodcastsListPresenter;
import com.example.testapp.view.activity.MainActivity;
import com.example.testapp.view.fragments.details.MovieDetailsFragment;
import com.example.testapp.view.fragments.details.PodcastDetailsFragment;
import com.example.testapp.view.fragments.list.AudioBooksListFragment;
import com.example.testapp.view.fragments.list.FavoritesListFragment;
import com.example.testapp.view.fragments.list.MoviesListFragment;
import com.example.testapp.view.fragments.list.PodcastsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, PresenterModule.class, ModelModule.class})
@Singleton
public interface AppComponent {
    //Methods for creation submodules:
    //Component createComponent(Module1 module1, Module1 module2...);

    AudioBookDetailsViewComponent createAudioBookDetailsViewComponent(AudioBookDetailsViewModule module);


    //Classes to be injected. All of them use objects provided by AppModule, PresenterModule and ModelModule
    void inject(MainActivity param);

    void inject(Model param);

    void inject(SqlImpl param);

    void inject(Repository param);

    //PRESENTERS
    void inject(AudioBooksListPresenter param);

    void inject(AudioBookDetailsPresenter param);

    void inject(MoviesListPresenter param);

    void inject(MovieDetailsPresenter param);

    void inject(PodcastsListPresenter param);

    void inject(PodcastDetailsPresenter param);

    void inject(FavoritesListPresenter param);

    //FRAGMENTS
    void inject(AudioBooksListFragment param);

    void inject(MoviesListFragment param);

    void inject(MovieDetailsFragment param);

    void inject(PodcastsListFragment param);

    void inject(PodcastDetailsFragment param);

    void inject(FavoritesListFragment param);

}