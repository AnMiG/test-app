package com.example.testapp.dependedncy_injection;

import com.example.testapp.model.net.IApiInterface;
import com.example.testapp.model.sql.ISqLiteInterface;
import com.example.testapp.model.sql.SqlImpl;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ModelModule {

    @Provides
    @Singleton
    IApiInterface provideApiInterface() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(Constants.API_BASE_URL)
                .build();
        return retrofit.create(IApiInterface.class);
    }

    @Provides
    @Singleton
    ISqLiteInterface provideSqlInterface() {
        return new SqlImpl();
    }
}
