package com.example.testapp.dependedncy_injection;

/**
 * Created by anmig on 11.06.2017.
 */

public class Constants {
    public static final String API_BASE_URL = "https://itunes.apple.com/";
    public static final String COUNTRY_CODE = "us";
    public static final String OBJECTS_LIMIT = "25";
}
