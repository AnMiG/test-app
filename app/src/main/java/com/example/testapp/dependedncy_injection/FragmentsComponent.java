package com.example.testapp.dependedncy_injection;

import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;

import dagger.Component;

@FragmentScope
@Component(dependencies = AppComponent.class, modules = AudioBookDetailsViewModule.class)

public interface FragmentsComponent {
    void inject(IAudioBookDetailsContract.View fragment);
}
