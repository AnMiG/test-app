package com.example.testapp.dependedncy_injection;

import android.app.Application;

import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;

import anmig.utils.T;

/**
 * The entry point of the app.
 */

public class App extends Application {
    private static AppComponent appComponent;
    private static AudioBookDetailsViewComponent audioBookDetailsViewComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this.getApplicationContext()))
                .modelModule(new ModelModule())
                .presenterModule(new PresenterModule())
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    //========================= A U D I O B O O K S == L I S T =====================================

    //======================= A U D I O B O O K == D E T A I L S ===================================

    public static AudioBookDetailsViewComponent createAudioBookDetailsViewComponent(IAudioBookDetailsContract.View view) {
        // always get only one instance
        if (audioBookDetailsViewComponent == null) {
            // start lifecycle of chatComponent
            audioBookDetailsViewComponent = appComponent.createAudioBookDetailsViewComponent(new AudioBookDetailsViewModule(view));
            return audioBookDetailsViewComponent;
        } else {
            T.e("AudioBookDetailsViewComponent not destroyed sinse last creation. Call clearAudioBookDetailsViewComponent() within the onSptop() method of your fragment.");
        }
        return null;
    }

    public static void clearAudioBookDetailsViewComponent() {
        // end lifecycle of chatComponent
        audioBookDetailsViewComponent = null;
    }


}
