package com.example.testapp.dependedncy_injection;

import com.example.testapp.model.IModel;
import com.example.testapp.model.Model;
import com.example.testapp.presenter.IRepository;
import com.example.testapp.presenter.Repository;
import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;
import com.example.testapp.presenter.presenters.AudioBookDetailsPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class PresenterModule {
    @Provides
    @Singleton
    IRepository provideRepository() {
        return new Repository();
    }

    @Provides
    @Singleton
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    IModel provideModel() {
        return new Model();
    }
}
