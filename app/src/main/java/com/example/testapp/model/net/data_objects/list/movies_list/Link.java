
package com.example.testapp.model.net.data_objects.list.movies_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Link {

    @SerializedName("attributes")
    @Expose
    private Attributes____ attributes;
    @SerializedName("im:duration")
    @Expose
    private ImDuration imDuration;

    public Attributes____ getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes____ attributes) {
        this.attributes = attributes;
    }

    public ImDuration getImDuration() {
        return imDuration;
    }

    public void setImDuration(ImDuration imDuration) {
        this.imDuration = imDuration;
    }

}
