package com.example.testapp.model.net;

import com.example.testapp.model.net.data_objects.details.audio_book_details.AudioBookDetailsDO;
import com.example.testapp.model.net.data_objects.details.movie_details.MovieDetailsDO;
import com.example.testapp.model.net.data_objects.details.podcast_details.PodcastDetailsDO;
import com.example.testapp.model.net.data_objects.list.audio_books_list.AudioBooksListDO;
import com.example.testapp.model.net.data_objects.list.movies_list.MoviesListDO;
import com.example.testapp.model.net.data_objects.list.podcasts_list.PodcastsListDO;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Interface for getting objects from REST API
 */

public interface IApiInterface {

    @GET("{country_code}/rss/topaudiobooks/limit={limit}/json")
    Observable<AudioBooksListDO> getAudioBooksData(@Path("country_code") String countryCode, @Path("limit") String limit);

    @GET("{country_code}/rss/topmovies/limit={limit}/json")
    Observable<MoviesListDO> getMoviesData(@Path("country_code") String countryCode, @Path("limit") String limit);

    @GET("{country_code}/rss/toppodcasts/limit={limit}/json")
    Observable<PodcastsListDO> getPodcastsData(@Path("country_code") String countryCode, @Path("limit") String limit);

    @GET("lookup")
    Observable<AudioBookDetailsDO> getAudioBooksDetailsData(@Query("id") String id);

    @GET("lookup")
    Observable<MovieDetailsDO> getMoviesDetailsData(@Query("id") String id);

    @GET("lookup")
    Observable<PodcastDetailsDO> getPodcastsDetailsData(@Query("id") String id);
}
