
package com.example.testapp.model.net.data_objects.list.movies_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes_____ {

    @SerializedName("im:id")
    @Expose
    private String imId;

    public String getImId() {
        return imId;
    }

    public void setImId(String imId) {
        this.imId = imId;
    }

}
