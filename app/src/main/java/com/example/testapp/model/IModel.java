package com.example.testapp.model;

import com.example.testapp.model.net.IApiInterface;
import com.example.testapp.model.sql.ISqLiteInterface;

/**
 * Created by anmig on 11.06.2017.
 */

public interface IModel {
    IApiInterface getApi();

    ISqLiteInterface getSql();
}
