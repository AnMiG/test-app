package com.example.testapp.model;

import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.model.net.IApiInterface;
import com.example.testapp.model.sql.ISqLiteInterface;

import javax.inject.Inject;

public class Model implements IModel {
    @Inject
    protected IApiInterface mApiInterface;

    @Inject
    protected ISqLiteInterface mSqLiteInterface;

    public Model() {
        App.getAppComponent().inject(this);
    }

    @Override
    public IApiInterface getApi() {
        return this.mApiInterface;
    }

    @Override
    public ISqLiteInterface getSql() {
        return this.mSqLiteInterface;
    }
}
