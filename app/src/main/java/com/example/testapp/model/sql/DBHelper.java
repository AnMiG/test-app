package com.example.testapp.model.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by anmig on 01.06.2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    public DBHelper(Context context, String name, int version) {
        super(context, name, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_AUDIO_BOOKS_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");
        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_MOVIES_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");
        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_PODCASTS_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");

        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_AUDIO_BOOK_DETAILS_TABLE_NAME+ " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");
        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_MOVIE_DETAILS_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");
        db.execSQL("CREATE TABLE " + SqlImpl.CACHED_PODCAST_DETAILS_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.JSON + " TEXT);");

        db.execSQL("CREATE TABLE " + SqlImpl.FAVORITES_TABLE_NAME + " (" + SqlImpl.ID + " TEXT, " + SqlImpl.CATEGORY + " TEXT, " + SqlImpl.JSON + " TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // ON UPGRADE DATABASE
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
        // onCreate(db);
    }
}
