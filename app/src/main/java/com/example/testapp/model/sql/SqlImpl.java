package com.example.testapp.model.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.testapp.dependedncy_injection.App;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * All manipulations with SQL mDatabase happens here
 * There is 7 tables:
 * 1. Cached audiobooks |id:String|json:String|
 * 2. Cached movies     |id:String|json:String|
 * 3. Cached podcasts   |id:String|json:String|
 * <p>
 * 1. Cached audiobook details |id:String|json:String|
 * 2. Cached movie details     |id:String|json:String|
 * 3. Cached podcast details   |id:String|json:String|
 * <p>
 * 7. Favorites        |id:String|category:String|json:String|
 */

public class SqlImpl implements ISqLiteInterface {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "test_app_db";

    static final String CACHED_AUDIO_BOOKS_TABLE_NAME = "cached_audio_books";
    static final String CACHED_MOVIES_TABLE_NAME = "cached_movies";
    static final String CACHED_PODCASTS_TABLE_NAME = "cached_podcasts";
    static final String CACHED_AUDIO_BOOK_DETAILS_TABLE_NAME = "cached_audio_book_details";
    static final String CACHED_MOVIE_DETAILS_TABLE_NAME = "cached_movie_details";
    static final String CACHED_PODCAST_DETAILS_TABLE_NAME = "cached_podcast_details";
    static final String FAVORITES_TABLE_NAME = "favorites";

    static final String ID = "id";
    static final String JSON = "json";
    static final String CATEGORY = "category";

    static final String CATEGORY_AUDIO_BOOKS = "audio_books";
    static final String CATEGORY_MOVIES = "movies";
    static final String CATEGORY_PODCASTS = "podcasts";

    private DBHelper mDbHelper;
    private SQLiteDatabase mDatabase;

    @Inject
    protected Context mContext;

    public SqlImpl() {
        App.getAppComponent().inject(this);
    }

    @Override
    public List<String> getCachedAudioBooks() {
        return this.getListOfItems(CACHED_AUDIO_BOOKS_TABLE_NAME);
    }

    @Override
    public List<String> getCachedMovies() {
        return this.getListOfItems(CACHED_MOVIES_TABLE_NAME);
    }

    @Override
    public List<String> getCachedPodcasts() {
        return this.getListOfItems(CACHED_PODCASTS_TABLE_NAME);
    }

    @Override
    public String getCachedAudioBookDetails(String id) throws Exception {
        return this.getDetailsOfItem(CACHED_AUDIO_BOOK_DETAILS_TABLE_NAME, id);
    }

    @Override
    public String getCachedMovieDetails(String id) throws Exception {
        return this.getDetailsOfItem(CACHED_MOVIE_DETAILS_TABLE_NAME, id);
    }

    @Override
    public String getCachedPodcastDetails(String id) throws Exception {
        return this.getDetailsOfItem(CACHED_PODCAST_DETAILS_TABLE_NAME, id);
    }

    @Override
    public List<String> getFavoriteAudioBooks() {
        return this.getFavoriteItems(CATEGORY_AUDIO_BOOKS);
    }

    @Override
    public List<String> getFavoriteMovies() {
        return this.getFavoriteItems(CATEGORY_MOVIES);
    }

    @Override
    public List<String> getFavoritePodcasts() {
        return this.getFavoriteItems(CATEGORY_PODCASTS);
    }

    /**
     * Saves list of view objects to cache
     *
     * @param list - Map<Key, Value> = <id, json>
     */
    @Override
    public void addAudioBooksToCache(Map<String, String> list) {
        this.addToCache(list, CACHED_AUDIO_BOOKS_TABLE_NAME);
    }

    /**
     * Saves list of view objects to cache
     *
     * @param list - Map<Key, Value> = <id, json>
     */
    @Override
    public void addMoviesToCache(Map<String, String> list) {
        this.addToCache(list, CACHED_MOVIES_TABLE_NAME);
    }

    /**
     * Saves list of view objects to cache
     *
     * @param list - Map<Key, Value> = <id, json>
     */
    @Override
    public void addPodcastsToCache(Map<String, String> list) {
        this.addToCache(list, CACHED_PODCASTS_TABLE_NAME);
    }

    @Override
    public void addAudioBookDetailsToCache(String id, String json) {
        this.addToCache(id, json, CACHED_AUDIO_BOOK_DETAILS_TABLE_NAME);
    }

    @Override
    public void addMovieDetailsToCache(String id, String json) {
        this.addToCache(id, json, CACHED_MOVIE_DETAILS_TABLE_NAME);
    }

    @Override
    public void addPodcastDetailsToCache(String id, String json) {
        this.addToCache(id, json, CACHED_PODCAST_DETAILS_TABLE_NAME);
    }

    @Override
    public boolean isAddedToFavorites(String id) {
        return this.checkIsDataAlreadyInDBorNot(FAVORITES_TABLE_NAME, ID, id);
    }

    @Override
    public void addAudioBooksToFavorites(String id, String json) {
        this.addToFavorites(id, json, CATEGORY_AUDIO_BOOKS);
    }

    @Override
    public void addMoviesToFavorites(String id, String json) {
        this.addToFavorites(id, json, CATEGORY_MOVIES);
    }

    @Override
    public void addPodcastsToFavorites(String id, String json) {
        this.addToFavorites(id, json, CATEGORY_PODCASTS);
    }

    @Override
    public void removeFromFavorites(String id) {
        this.open();
        this.mDatabase.delete(FAVORITES_TABLE_NAME, ID + " = ?", new String[]{id});
        this.close();
    }

    @Override
    public void open() {
        if (this.mDbHelper != null)
            return;
        this.mDbHelper = new DBHelper(mContext, DATABASE_NAME, DATABASE_VERSION);
        this.mDatabase = this.mDbHelper.getWritableDatabase();
    }

    @Override
    public void close() {
        if (this.mDbHelper != null)
            this.mDbHelper.close();
        if (this.mDatabase != null)
            this.mDatabase.close();
        this.mDbHelper = null;
        this.mDatabase = null;
    }

    private List<String> getListOfItems(String tableName) {
        this.open();
        List<String> list = null;
        Cursor cursor = this.mDatabase.query(tableName, null, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            list = new ArrayList<>();
            for (int i = 0; i < cursor.getCount(); i++) {
                list.add(cursor.getString(1));
                cursor.moveToNext();
            }
            cursor.close();
        }
        this.close();
        return list;
    }

    private String getDetailsOfItem(String tableName, String id) {
        String Query = "Select * from " + tableName + " where " + ID + "=?";
        Cursor cursor = this.mDatabase.rawQuery(Query, new String[]{id});
        String str = null;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            str = cursor.getString(1);
            cursor.close();
        }
        return str;
    }

    private List<String> getFavoriteItems(String pCategory) {
        List<String> list = null;
        String Query = "Select * from " + FAVORITES_TABLE_NAME + " where " + CATEGORY + "=?";
        Cursor cursor = this.mDatabase.rawQuery(Query, new String[]{pCategory});
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            list = new ArrayList<>();
            for (int i = 0; i < cursor.getCount(); i++) {
                list.add(cursor.getString(2));
                cursor.moveToNext();
            }
            cursor.close();
        }
        return list;
    }

    private void addToCache(Map<String, String> list, String tableName) {
        this.open();
        this.mDatabase.delete(tableName, null, null);
        for (Map.Entry<String, String> entry : list.entrySet()) {
            ContentValues cv = new ContentValues();
            cv.put(ID, entry.getKey());
            cv.put(JSON, entry.getValue());
            this.mDatabase.insert(tableName, null, cv);
        }
        this.close();
    }

    private void addToCache(String id, String json, String tableName) {
        this.open();
        ContentValues cv = new ContentValues();
        cv.put(ID, id);
        cv.put(JSON, json);
        int rows = this.mDatabase.update(tableName, cv, ID + " = " + id, null);
        if (rows == 0) {
            this.mDatabase.insert(tableName, null, cv);
        }
        this.close();
    }

    private void addToFavorites(String id, String json, String category) {
        this.open();
        ContentValues cv = new ContentValues();
        cv.put(ID, id);
        cv.put(CATEGORY, category);
        cv.put(JSON, json);
        this.mDatabase.insert(FAVORITES_TABLE_NAME, null, cv);
        this.close();
    }

    private boolean checkIsDataAlreadyInDBorNot(String tableName, String dbfield, String fieldValue) {
        String Query = "Select * from " + tableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = this.mDatabase.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}
