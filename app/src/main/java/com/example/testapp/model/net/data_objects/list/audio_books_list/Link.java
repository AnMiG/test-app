
package com.example.testapp.model.net.data_objects.list.audio_books_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Link {

    @SerializedName("attributes")
    @Expose
    private Attributes___ attributes;
    @SerializedName("im:duration")
    @Expose
    private ImDuration imDuration;

    public Attributes___ getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes___ attributes) {
        this.attributes = attributes;
    }

    public ImDuration getImDuration() {
        return imDuration;
    }

    public void setImDuration(ImDuration imDuration) {
        this.imDuration = imDuration;
    }

}
