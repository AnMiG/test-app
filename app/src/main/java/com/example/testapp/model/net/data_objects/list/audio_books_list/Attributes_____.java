
package com.example.testapp.model.net.data_objects.list.audio_books_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes_____ {

    @SerializedName("term")
    @Expose
    private String term;
    @SerializedName("label")
    @Expose
    private String label;

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
