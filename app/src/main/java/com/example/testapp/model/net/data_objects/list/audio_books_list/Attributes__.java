
package com.example.testapp.model.net.data_objects.list.audio_books_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes__ {

    @SerializedName("href")
    @Expose
    private String href;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
