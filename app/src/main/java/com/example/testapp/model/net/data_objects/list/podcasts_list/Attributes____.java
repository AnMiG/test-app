
package com.example.testapp.model.net.data_objects.list.podcasts_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes____ {

    @SerializedName("im:id")
    @Expose
    private String imId;

    public String getImId() {
        return imId;
    }

    public void setImId(String imId) {
        this.imId = imId;
    }

}
