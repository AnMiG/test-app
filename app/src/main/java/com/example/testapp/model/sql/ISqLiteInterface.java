package com.example.testapp.model.sql;

import java.util.List;
import java.util.Map;

/**
 * Interface for getting objects from SQLite database
 * 1. Get cahed data and favourites
 * 2. Set chched data to database
 * 3. Set favourites to database
 */

public interface ISqLiteInterface {

    List<String> getCachedAudioBooks() throws Exception;

    List<String> getCachedMovies() throws Exception;

    List<String> getCachedPodcasts() throws Exception;


    String getCachedAudioBookDetails(String id) throws Exception;

    String getCachedMovieDetails(String id) throws Exception;

    String getCachedPodcastDetails(String id) throws Exception;


    List<String> getFavoriteAudioBooks() throws Exception;

    List<String> getFavoriteMovies() throws Exception;

    List<String> getFavoritePodcasts() throws Exception;


    void addAudioBooksToCache(Map<String, String> list);

    void addMoviesToCache(Map<String, String> list);

    void addPodcastsToCache(Map<String, String> list);

    void addAudioBookDetailsToCache(String id, String json);

    void addMovieDetailsToCache(String id, String json);

    void addPodcastDetailsToCache(String id, String json);


    boolean isAddedToFavorites(String id);

    void addAudioBooksToFavorites(String id, String json) throws Exception;

    void addMoviesToFavorites(String id, String json) throws Exception;

    void addPodcastsToFavorites(String id, String json) throws Exception;

    void removeFromFavorites(String id) throws Exception;

    void open();

    void close();
}
