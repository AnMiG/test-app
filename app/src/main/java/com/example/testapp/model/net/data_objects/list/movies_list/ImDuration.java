
package com.example.testapp.model.net.data_objects.list.movies_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImDuration {

    @SerializedName("label")
    @Expose
    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
