package com.example.testapp.model.net.data_objects.list.audio_books_list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AudioBooksListDO {

    @SerializedName("feed")
    @Expose
    private Feed feed;

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}
