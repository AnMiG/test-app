package com.example.testapp.view.fragments.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.contracts.IAudioBookDetailsContract;
import com.example.testapp.presenter.presenters.AudioBookDetailsPresenter;
import com.example.testapp.presenter.view_objects.details.AudioBookDetailsVO;
import com.example.testapp.view.navigation_strategy.IFragmentsNavigationStrategy;
import com.example.testapp.view.activity.MainActivity;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.view.fragments.BaseDetailsFragment;
import com.example.testapp.view.navigation_strategy.FragmentsNavigationStrategy;
import com.google.gson.Gson;

import javax.inject.Inject;

import anmig.utils.DownloadCircularAndBluredImageTask;
import anmig.utils.T;

public class AudioBookDetailsFragment extends BaseDetailsFragment implements IAudioBookDetailsContract.View {
    @Inject
    protected IFragmentsNavigationStrategy mFragmentsNavigationStrategy;

    @Inject
    protected AudioBookDetailsPresenter mPresenter;

    private AudioBooksListVO mListVO;
    private String mReceivedJson;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.createAudioBookDetailsViewComponent(this).inject(this);
        this.mReceivedJson = this.getArguments().getString(JSON_KEY);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        this.updateDetails(mReceivedJson);
    }

    @Override
    public void updateDetails(String viewObjectJson) {
        Gson gson = new Gson();
        this.mListVO = gson.fromJson(viewObjectJson, AudioBooksListVO.class);
        this.mPresenter.getDetails(this.mListVO.getId());
    }

    @Override
    public void showLoading(boolean state) {
        this.mErrorMessageContainer.setVisibility(View.GONE);
        this.mRefreshLayout.setRefreshing(state);
    }

    @Override
    public void showDetails(AudioBookDetailsVO details) {
        super.showDetails();

        new DownloadCircularAndBluredImageTask(this.getContext(), this.mIimage, this.mImageBackground,
                R.drawable.ic_no_image, ContextCompat.getColor(this.getContext(), MainActivity.DETAILS_BACKGROUND_COLORS[this.getColorIndex()]))
                .execute(new DownloadCircularAndBluredImageTask.Params(details.getImage(), details.getImageWidth(), details.getImageHeight()));

        StringBuilder sb = new StringBuilder();

        sb.append(" ").append(details.getName());
        this.mTitleText.setText(sb);
        sb.setLength(0);

        sb.append(getContext().getString(R.string.title_author)).append("    ").append(details.getArtist());
        this.mAuthorText.setText(sb);
        sb.setLength(0);

        sb.append(getContext().getString(R.string.title_genre)).append("    ").append(details.getGenre());
        this.mGenreText.setText(sb);
        sb.setLength(0);

        sb.append(getContext().getString(R.string.title_price)).append("    ").append(details.getPrice()).append("  ")
                .append(details.getCurrency());
        this.mPriceText.setText(sb);
        this.updateMenu();
    }

    @Override
    public void showAddedToFavorites() {
        this.mListVO.setAddedToFavorites(true);
        this.mFragmentsNavigationStrategy.onDetailsDataUpdated(new Gson().toJson(this.mListVO));
        this.updateMenu();
        Toast.makeText(this.getContext(), R.string.message_favorites_added, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRemovedFromFavorites() {
        this.mListVO.setAddedToFavorites(false);
        this.mFragmentsNavigationStrategy.onDetailsDataUpdated(new Gson().toJson(this.mListVO));
        this.updateMenu();
        Toast.makeText(this.getContext(), R.string.message_favorites_removed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String message) {
        super.showError(message);
    }

    @Override
    public int getColorIndex() {
        return FragmentsNavigationStrategy.STATE_AUDIO_BOOKS;
    }

    @Override
    public void onOptionsItemSelected(int itemId) {
        if (itemId == R.id.action_favorite || itemId == R.id.action_settings) {
            MenuItem[] menuItems = this.mFragmentListener.getMenuItems();
            if (menuItems == null) return;
            if (this.mListVO.isAddedToFavorites()) {
                this.mPresenter.removeFromFavorites(this.mListVO);
            } else {
                this.mPresenter.addToFavorites(this.mListVO);
            }
        }
    }

    @Override
    public int getTitle() {
        return R.string.title_audio_books;
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mPresenter.onStop();
        App.clearAudioBookDetailsViewComponent();
    }

    private void updateMenu() {
        MenuItem[] menuItems = this.mFragmentListener.getMenuItems();
        if (menuItems == null) return;

        for (int i = 0; i < menuItems.length; i++) {
            menuItems[i].setVisible(true);
        }
        if (this.mListVO.isAddedToFavorites()) {
            menuItems[0].setIcon(R.drawable.ic_star_white_24dp);
            menuItems[1].setTitle(R.string.action_remove_from_favorites);
        } else {
            menuItems[0].setIcon(R.drawable.ic_star_border_white_24dp);
            menuItems[1].setTitle(R.string.action_add_to_favorites);
        }
    }
}
