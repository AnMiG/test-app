package com.example.testapp.view.adapters.favorites;

import com.bignerdranch.expandablerecyclerview.model.Parent;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

import java.util.List;

import anmig.utils.T;

/**
 * Created by anmig on 05.06.2017.
 */

public class FavoritesParent implements Parent<ListViewObject> {
    private String name;
    private List<ListViewObject> mObjects;

    public FavoritesParent(String pName, Object pObjects) {
        this.mObjects = (List<ListViewObject>) pObjects;
        this.name = pName;
    }

    public String getName() {
        return name;
    }

    @Override
    public List<ListViewObject> getChildList() {
        return mObjects;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
