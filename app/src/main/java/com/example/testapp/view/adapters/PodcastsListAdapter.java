package com.example.testapp.view.adapters;

import android.content.Context;

import com.example.testapp.presenter.view_objects.list.PodcastsListVO;

import java.util.List;

import anmig.utils.cache.ImageFetcher;

/**
 * Created by anmig on 02.06.2017.
 */

public class PodcastsListAdapter extends BaseListAdapter<PodcastsListVO> {
    public PodcastsListAdapter(Context pContext, List<PodcastsListVO> pDataObjects, ImageFetcher pImageFetcher, IItemSelectedListener pItemSelectedListener) {
        super(pContext, pDataObjects, pImageFetcher, pItemSelectedListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        PodcastsListVO object = this.mObjects.get(i);
        viewHolder.setIndex(i);
        if (object.getImage() != null)
            viewHolder.setImage(object.getImage());
        viewHolder.setTitle(object.getTitle());
        viewHolder.setArtist(object.getArtist());
        viewHolder.setaddedToFavourites(object.isAddedToFavorites());
    }
}