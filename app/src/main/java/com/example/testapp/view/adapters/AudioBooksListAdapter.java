package com.example.testapp.view.adapters;

import android.content.Context;

import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;

import java.util.List;

import anmig.utils.cache.ImageFetcher;

/**
 * Created by anmig on 02.06.2017.
 */

public class AudioBooksListAdapter extends BaseListAdapter<AudioBooksListVO> {

    public AudioBooksListAdapter(Context pContext, List<AudioBooksListVO> pDataObjects, ImageFetcher pImageFetcher, IItemSelectedListener pItemSelectedListener) {
        super(pContext, pDataObjects, pImageFetcher, pItemSelectedListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        AudioBooksListVO object = this.mObjects.get(i);
        viewHolder.setIndex(i);
        if (object.getImage() != null)
            viewHolder.setImage(object.getImage());
        viewHolder.setTitle(object.getTitle());
        viewHolder.setArtist(object.getArtist());
        viewHolder.setaddedToFavourites(object.isAddedToFavorites());
    }
}