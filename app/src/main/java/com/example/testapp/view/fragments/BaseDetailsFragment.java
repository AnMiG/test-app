package com.example.testapp.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.testapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anmig on 04.06.2017.
 */

public abstract class BaseDetailsFragment extends BaseFragment {
    @BindView(R.id.details_container)
    protected RelativeLayout mDetailsContainer;

    @BindView(R.id.details_image)
    protected ImageView mIimage;

    @BindView(R.id.details_image_background)
    protected ImageView mImageBackground;

    @BindView(R.id.details_swipe_container)
    protected SwipeRefreshLayout mRefreshLayout;

    @BindView(R.id.error_message_container)
    protected LinearLayout mErrorMessageContainer;

    @BindView(R.id.details_title)
    protected TextView mTitleText;

    @BindView(R.id.details_author_text)
    protected TextView mAuthorText;

    @BindView(R.id.details_genre_text)
    protected TextView mGenreText;

    @BindView(R.id.details_price_text)
    protected TextView mPriceText;

    @BindView(R.id.error_message_text)
    protected TextView mErrorMessageText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details_layout, container, false);
        ButterKnife.bind(this, view);
        this.mRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        this.mRefreshLayout.setOnRefreshListener(this);
        this.mErrorMessageContainer.setVisibility(View.GONE);
        return view;
    }

    public abstract void updateDetails(String viewObjectJson);


    @Override
    public void onRefresh() {
        this.mRefreshLayout.setRefreshing(false);
    }

    public void showDetails() {
        this.mDetailsContainer.setVisibility(View.VISIBLE);
        this.mErrorMessageContainer.setVisibility(View.GONE);
    }

    protected void showError(String message) {
        this.mDetailsContainer.setVisibility(View.GONE);
        this.mErrorMessageContainer.setVisibility(View.VISIBLE);
        this.mErrorMessageText.setText(message);
    }
}
