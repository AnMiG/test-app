package com.example.testapp.view.adapters;

/**
 * Created by anmig on 05.06.2017.
 */

public interface IFavoriteItemSelectedListener {
    void onFavoritesPressed(int parentIndex, int itemIndex);

    void onItemSelected(int parentIndex, int itemIndex);
}