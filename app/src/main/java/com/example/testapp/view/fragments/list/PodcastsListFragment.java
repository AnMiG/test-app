package com.example.testapp.view.fragments.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.contracts.IPodcastsListContract;
import com.example.testapp.presenter.presenters.PodcastsListPresenter;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.example.testapp.view.navigation_strategy.IFragmentsNavigationStrategy;
import com.example.testapp.view.adapters.IItemSelectedListener;
import com.example.testapp.view.adapters.PodcastsListAdapter;
import com.example.testapp.view.fragments.BaseListFragment;
import com.example.testapp.view.navigation_strategy.FragmentsNavigationStrategy;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by anmig on 31.05.2017.
 */

public class PodcastsListFragment extends BaseListFragment implements IPodcastsListContract.View, IItemSelectedListener {
    @Inject
    protected IFragmentsNavigationStrategy mFragmentsNavigationStrategy;

    public static final String PODCASTS_LIST_KEY = "podcasts_list_key";
    private IPodcastsListContract.Presenter mPresenter;
    private List<PodcastsListVO> mObjects;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        if (savedInstanceState != null) {
            this.mObjects = (List<PodcastsListVO>) savedInstanceState.getSerializable(PODCASTS_LIST_KEY);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        this.mPresenter = new PodcastsListPresenter(this);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        if (this.isObjectsEmpty()) {
            this.mObjects = new ArrayList<>();
            this.mRecyclerView.setAdapter(new PodcastsListAdapter(this.getContext(), mObjects, this.mImageFetcher, this));
            this.loadList();
        } else {
            this.mRecyclerView.setAdapter(new PodcastsListAdapter(this.getContext(), mObjects, this.mImageFetcher, this));
            this.mRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (!isObjectsEmpty()) {
            outState.putSerializable(PODCASTS_LIST_KEY, new ArrayList<>(mObjects));
        }
    }

    private boolean isObjectsEmpty() {
        return this.mObjects == null || this.mObjects.isEmpty();
    }

    @Override
    public void loadList() {
        this.mPresenter.getList();
    }

    @Override
    public int getTitle() {
        return R.string.title_podcasts;
    }

    @Override
    public void showLoading(boolean state) {
        this.mErrorMessageContainer.setVisibility(View.GONE);
        this.mRefreshLayout.setRefreshing(state);
    }

    @Override
    public void showList(List<PodcastsListVO> list) {
        super.showList();
        this.mObjects.clear();
        this.mObjects.addAll(list);
        this.mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void showError(String message) {
        super.showError(message);
    }

    @Override
    public void showDetailsFragment(int index) {
        Gson gson = new Gson();
        this.mFragmentsNavigationStrategy.showDetailsFragment(FragmentsNavigationStrategy.STATE_PODCASTS, FragmentsNavigationStrategy.STATE_PODCASTS, gson.toJson(this.mObjects.get(index)));
    }

    @Override
    public void showAddedToFavorites() {
        this.mRecyclerView.getAdapter().notifyDataSetChanged();
        Toast.makeText(this.getContext(), R.string.message_favorites_added, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRemovedFromFavorites() {
        this.mRecyclerView.getAdapter().notifyDataSetChanged();
        Toast.makeText(this.getContext(), R.string.message_favorites_removed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        this.mPresenter.getList();
    }

    @Override
    public void onFavoritesPressed(int itemIndex) {
        PodcastsListVO vo = this.mObjects.get(itemIndex);
        if (vo.isAddedToFavorites())
            this.mPresenter.removeFromFavorites(vo);
        else
            this.mPresenter.addToFavorites(vo);
    }

    @Override
    public void onItemSelected(int itemIndex) {
        this.mPresenter.onItemSelected(itemIndex);
    }

    @Override
    public int getColorIndex() {
        return FragmentsNavigationStrategy.STATE_PODCASTS;
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mPresenter.onStop();
    }
}
