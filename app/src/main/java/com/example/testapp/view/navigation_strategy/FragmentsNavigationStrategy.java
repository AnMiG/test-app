package com.example.testapp.view.navigation_strategy;

import android.os.Bundle;

import com.example.testapp.R;
import com.example.testapp.view.activity.ISwitchFragmentListener;
import com.example.testapp.view.fragments.BaseFragment;
import com.example.testapp.view.fragments.details.AudioBookDetailsFragment;
import com.example.testapp.view.fragments.details.MovieDetailsFragment;
import com.example.testapp.view.fragments.details.PodcastDetailsFragment;
import com.example.testapp.view.fragments.list.AudioBooksListFragment;
import com.example.testapp.view.fragments.list.FavoritesListFragment;
import com.example.testapp.view.fragments.list.MoviesListFragment;
import com.example.testapp.view.fragments.list.PodcastsListFragment;

import java.util.ArrayList;

/**
 * The strategy of application navigation. As described:
 * When you tap on item of the currently selected category it should be opened on another page with
 * some detail information (to get the detail information use the following link :
 * http://itunes.apple.com/lookup?id={entry_id_attributes_im:id}).
 * If you select another category by tapping on the BottomNavigationView, the state of the current
 * view should be stored. E.g. If i open AudioBooks category, select an item, switch to Podcasts and
 * press the back navigation button, the app should navigate back to the previously selected item
 * from the AudioBooks category.
 */

public class FragmentsNavigationStrategy implements IFragmentsNavigationStrategy {

    public static final int STATE_AUDIO_BOOKS = 0;
    public static final int STATE_MOVIES = 1;
    public static final int STATE_PODCASTS = 2;
    public static final int STATE_FAVORITES = 3;

    public static final int CACHE_MAX_SIZE = 5;

    private ArrayList<Integer> mCache;
    private AppState[] mStates;
    private ISwitchFragmentListener mSwitchFragmentListener;
    private int mCurrentState = -1;
    private boolean mPendingAddToCache = false;

    public FragmentsNavigationStrategy() {
    }

    public void init(ISwitchFragmentListener pSwitchFragmentListener) {
        if (this.mSwitchFragmentListener != null) {
            return;
        }

        this.mSwitchFragmentListener = pSwitchFragmentListener;

        this.mStates = new AppState[4];
        for (int i = 0; i < this.mStates.length; i++) {
            this.mStates[i] = new AppState(i);
        }

        this.mCache = new ArrayList<>();

        this.mSwitchFragmentListener.setNavigationItemSelected(R.id.navigation_audio_books);
    }

    public void onBackKeyPressed() {
        if (this.mCurrentState >= 0) {
            this.mPendingAddToCache = false;
            AppState currentState = this.mStates[mCurrentState];
            AppState newState = null;
            if (currentState.getType() == AppState.StateType.DETAILS) {
                //close details and open list
                currentState.setType(AppState.StateType.LIST);
                newState = currentState;
            } else {
                //open previous state
                if (this.mCache.size() > 0) {
                    newState = this.mStates[mCache.get(mCache.size() - 1)];
                    this.mCache.remove(mCache.size() - 1);
                } else {
                    this.mSwitchFragmentListener.closeApp();
                    return;
                }
            }
            switch (newState.getIndex()) {
                case STATE_AUDIO_BOOKS:
                    this.mSwitchFragmentListener.setNavigationItemSelected(R.id.navigation_audio_books);
                    break;
                case STATE_MOVIES:
                    this.mSwitchFragmentListener.setNavigationItemSelected(R.id.navigation_movies);
                    break;
                case STATE_PODCASTS:
                    this.mSwitchFragmentListener.setNavigationItemSelected(R.id.navigation_podcasts);
                    break;
                case STATE_FAVORITES:
                    this.mSwitchFragmentListener.setNavigationItemSelected(R.id.navigation_favourites);
                    break;
                default:
                    break;
            }
        } else {
            this.mSwitchFragmentListener.closeApp();
        }
    }

    public boolean onNavigationViewPressed(int id) {
        boolean changeActionBarColor = true;
        boolean changeNavigationBarColor = true;
        boolean changeTitle = true;

        BaseFragment fragment = null;
        AppState newState = null;

        if (mPendingAddToCache) {
            if (mCache.size() > CACHE_MAX_SIZE) this.mCache.remove(0);
            Integer val = -1;
            if (mCache.size() > 0) {
                val = this.mCache.get(this.mCache.size() - 1);
            }
            if (val != this.mStates[this.mCurrentState].getIndex())
                this.mCache.add(this.mStates[this.mCurrentState].getIndex());
        }
        this.mPendingAddToCache = true;

        switch (id) {
            case R.id.navigation_audio_books:
                newState = this.mStates[STATE_AUDIO_BOOKS];
                fragment = newState.getType() == AppState.StateType.LIST ? new AudioBooksListFragment() : new AudioBookDetailsFragment();
                break;
            case R.id.navigation_movies:
                newState = this.mStates[STATE_MOVIES];
                fragment = newState.getType() == AppState.StateType.LIST ? new MoviesListFragment() : new MovieDetailsFragment();
                break;
            case R.id.navigation_podcasts:
                newState = this.mStates[STATE_PODCASTS];
                fragment = newState.getType() == AppState.StateType.LIST ? new PodcastsListFragment() : new PodcastDetailsFragment();
                break;
            case R.id.navigation_favourites:
                newState = this.mStates[STATE_FAVORITES];
                if (newState.getType() == AppState.StateType.LIST) {
                    fragment = new FavoritesListFragment();
                } else {
                    changeActionBarColor = true;
                    changeNavigationBarColor = false;
                    changeTitle = true;
                    switch (newState.getDetailsIndex()) {
                        case STATE_AUDIO_BOOKS:
                            fragment = new AudioBookDetailsFragment();
                            break;
                        case STATE_MOVIES:
                            fragment = new MovieDetailsFragment();
                            break;
                        case STATE_PODCASTS:
                            fragment = new PodcastDetailsFragment();
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
        if (fragment != null) {
            if (newState.getType() == AppState.StateType.DETAILS) {
                Bundle args = new Bundle();
                args.putString(BaseFragment.JSON_KEY, newState.getDetailsObjectBundle());
                fragment.setArguments(args);
            }
            this.mCurrentState = newState.getIndex();
            this.mSwitchFragmentListener.changeFragment(fragment, changeActionBarColor, changeNavigationBarColor, changeTitle);
            return true;
        }
        return false;
    }

    public void showDetailsFragment(int stateFromIndex, int stateToIndex, String viewObjectJsonBundle) {
        boolean changeActionBarColor = false;
        boolean changeNavigationBarColor = false;
        boolean changeTitle = false;

        if (stateFromIndex == STATE_FAVORITES) {
            changeActionBarColor = true;
            changeTitle = true;
        }

        BaseFragment fragment = null;
        switch (stateToIndex) {
            case STATE_AUDIO_BOOKS:
                fragment = new AudioBookDetailsFragment();
                break;
            case STATE_MOVIES:
                fragment = new MovieDetailsFragment();
                break;
            case STATE_PODCASTS:
                fragment = new PodcastDetailsFragment();
                break;
            default:
                break;
        }
        if (fragment != null) {
            AppState currentState = this.mStates[mCurrentState];
            currentState.setDetailsObjectBundle(viewObjectJsonBundle);
            currentState.setType(AppState.StateType.DETAILS);
            currentState.setDetailsIndex(stateToIndex);

            Bundle args = new Bundle();
            args.putString(BaseFragment.JSON_KEY, viewObjectJsonBundle);
            fragment.setArguments(args);
            this.mSwitchFragmentListener.changeFragment(fragment, changeActionBarColor, changeNavigationBarColor, changeTitle);
        }
    }

    public void onDetailsDataUpdated(String viewObjectJsonBundle) {
        this.mStates[this.mCurrentState].setDetailsObjectBundle(viewObjectJsonBundle);
    }
}
