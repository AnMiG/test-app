package com.example.testapp.view.adapters.favorites;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.example.testapp.R;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.example.testapp.view.adapters.IFavoriteItemSelectedListener;
import com.example.testapp.view.navigation_strategy.FragmentsNavigationStrategy;

import java.util.List;

public class FavoritesAdapter extends ExpandableRecyclerAdapter<FavoritesParent, ListViewObject, FavoritesParentViewHolder, FavoritesViewHolder> {
    private LayoutInflater mInflater;
    protected Context mContext;
    protected IFavoriteItemSelectedListener mItemSelectedListener;

    public FavoritesAdapter(Context pContext, List<FavoritesParent> parentList, IFavoriteItemSelectedListener pItemSelectedListener) {
        super(parentList);
        this.mContext = pContext;
        this.mInflater = LayoutInflater.from(mContext);
        this.mItemSelectedListener = pItemSelectedListener;
    }

    @NonNull
    @Override
    public FavoritesParentViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.list_header, parentViewGroup, false);
        return new FavoritesParentViewHolder(view);
    }

    @NonNull
    @Override
    public FavoritesViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View view = mInflater.inflate(R.layout.list_item, childViewGroup, false);
        return new FavoritesViewHolder(view, mContext, mItemSelectedListener);
    }

    @Override
    public void onBindParentViewHolder(@NonNull FavoritesParentViewHolder parentViewHolder, int parentPosition, @NonNull FavoritesParent parent) {
        parentViewHolder.setHeaderText(this.getParentList().get(parentPosition).getName());
        switch (parentPosition) {
            case FragmentsNavigationStrategy.STATE_AUDIO_BOOKS:
                parentViewHolder.setHeaderBacgroune(R.drawable.list_category_0);
                break;
            case FragmentsNavigationStrategy.STATE_MOVIES:
                parentViewHolder.setHeaderBacgroune(R.drawable.list_category_1);
                break;
            case FragmentsNavigationStrategy.STATE_PODCASTS:
                parentViewHolder.setHeaderBacgroune(R.drawable.list_category_2);
                break;
        }
    }

    @Override
    public void onBindChildViewHolder(@NonNull FavoritesViewHolder viewHolder, int parentPosition, int childPosition, @NonNull ListViewObject child) {
        viewHolder.setParentIndex(parentPosition);
        viewHolder.setChildIndex(childPosition);
        switch (parentPosition) {
            case FragmentsNavigationStrategy.STATE_AUDIO_BOOKS:
                AudioBooksListVO ao = (AudioBooksListVO) this.getParentList().get(parentPosition).getChildList().get(childPosition);
                if (ao.getImage() != null)
                    viewHolder.setImage(ao.getImage());
                viewHolder.setTitle(ao.getTitle());
                viewHolder.setArtist(ao.getArtist());
                viewHolder.setaddedToFavourites(ao.isAddedToFavorites());
                break;
            case FragmentsNavigationStrategy.STATE_MOVIES:
                MoviesListVO mo = (MoviesListVO) this.getParentList().get(parentPosition).getChildList().get(childPosition);
                if (mo.getImage() != null)
                    viewHolder.setImage(mo.getImage());
                viewHolder.setTitle(mo.getTitle());
                viewHolder.setArtist(mo.getArtist());
                viewHolder.setaddedToFavourites(mo.isAddedToFavorites());
                break;
            case FragmentsNavigationStrategy.STATE_PODCASTS:
                PodcastsListVO po = (PodcastsListVO) this.getParentList().get(parentPosition).getChildList().get(childPosition);
                if (po.getImage() != null)
                    viewHolder.setImage(po.getImage());
                viewHolder.setTitle(po.getTitle());
                viewHolder.setArtist(po.getArtist());
                viewHolder.setaddedToFavourites(po.isAddedToFavorites());
                break;
        }
    }
}
