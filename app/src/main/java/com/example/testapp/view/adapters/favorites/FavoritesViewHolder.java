package com.example.testapp.view.adapters.favorites;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.example.testapp.R;
import com.example.testapp.view.adapters.IFavoriteItemSelectedListener;

import anmig.utils.DownloadCircularImageTask;

public class FavoritesViewHolder extends ChildViewHolder implements View.OnClickListener {
    private int mParentIndex;
    private int mChildIndex;
    private Context mContext;
    private IFavoriteItemSelectedListener mClickListener;
    private ImageView mImageView;
    private ImageButton mFavouriteImageButton;
    private TextView mTitle;
    private TextView mArtist;
    private RelativeLayout mBackground;

    public FavoritesViewHolder(View itemView, Context pContext, IFavoriteItemSelectedListener clickListener) {
        super(itemView);
        this.mContext = pContext;
        this.mClickListener = clickListener;

        this.mImageView = (ImageView) itemView.findViewById(R.id.list_item_image);
        this.mFavouriteImageButton = (ImageButton) itemView.findViewById(R.id.list_item_favourite_button);
        this.mTitle = (TextView) itemView.findViewById(R.id.list_item_title);
        this.mArtist = (TextView) itemView.findViewById(R.id.list_item_artist);
        this.mBackground = (RelativeLayout) itemView.findViewById(R.id.list_item_background);

        this.mFavouriteImageButton.setOnClickListener(this);
        this.itemView.setOnClickListener(this);
    }

    public void setImage(String url) {
        new DownloadCircularImageTask(mContext, mImageView, R.drawable.ic_no_image).execute(url);
    }

    public void setTitle(String text) {
        this.mTitle.setText(text);
    }

    public void setArtist(String text) {
        this.mArtist.setText(text);
    }

    public void hideFavoriteButton() {
        this.mFavouriteImageButton.setVisibility(View.GONE);
    }

    public void setaddedToFavourites(boolean val) {
        if (val) {
            this.mFavouriteImageButton.setImageDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.ic_star_white_24dp));
            this.mFavouriteImageButton.setColorFilter(ContextCompat.getColor(this.mContext, R.color.colorIsFavourites));
        } else {
            this.mFavouriteImageButton.setImageDrawable(ContextCompat.getDrawable(this.mContext, R.drawable.ic_star_border_white_24dp));
            this.mFavouriteImageButton.setColorFilter(ContextCompat.getColor(this.mContext, R.color.colorNotFavourites));
        }
    }

    public void setBackground(int res) {
        this.mBackground.setBackgroundResource(res);
    }

    public void setParentIndex(int pParentIndex) {
        this.mParentIndex = pParentIndex;
    }

    public void setChildIndex(int pChildIndex) {
        this.mChildIndex = pChildIndex;
    }

    @Override
    public void onClick(View v) {
        if (v == this.mFavouriteImageButton) {
            this.mClickListener.onFavoritesPressed(this.mParentIndex, this.mChildIndex);
        } else {
            this.mClickListener.onItemSelected(this.mParentIndex, this.mChildIndex);
        }
    }
}
