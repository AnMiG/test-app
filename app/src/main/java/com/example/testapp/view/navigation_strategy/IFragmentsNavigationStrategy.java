package com.example.testapp.view.navigation_strategy;

import com.example.testapp.view.activity.ISwitchFragmentListener;

public interface IFragmentsNavigationStrategy {
    void init(ISwitchFragmentListener pSwitchFragmentListener);

    void onBackKeyPressed();

    boolean onNavigationViewPressed(int id);

    void showDetailsFragment(int stateFromIndex, int stateToIndex, String viewObjectJsonBundle);

    void onDetailsDataUpdated(String viewObjectJsonBundle);

}
