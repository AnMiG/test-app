package com.example.testapp.view.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;

import com.example.testapp.R;
import com.example.testapp.view.activity.IFragmentsContainer;

import anmig.utils.cache.ImageCache;
import anmig.utils.cache.ImageFetcher;

/**
 * Created by anmig on 01.06.2017.
 */

public abstract class BaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String JSON_KEY = "json_view_object";
    private static final String IMAGE_CACHE_DIR = "thumbs";
    protected IFragmentsContainer mFragmentListener;
    protected ImageFetcher mImageFetcher;

    /**
     * @return - 0 - Audiobooks, 1 - Movies, 2 - Podcasts, 3 - Favorites
     */
    public abstract int getColorIndex();

    public abstract int getTitle();

    public abstract void onOptionsItemSelected(int itemId);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(getActivity(), IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(getActivity(), getResources().getDimensionPixelSize(R.dimen.list_item_image_size));

        //mImageFetcher.setLoadingImage(R.drawable.empty_photo);
        mImageFetcher.addImageCache(getActivity().getSupportFragmentManager(), cacheParams);

        //!!!must be called before .setLoadingImage()
        mImageFetcher.setCircle(true);

        mImageFetcher.setLoadingImage(R.drawable.ic_no_image);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            try {
                this.mFragmentListener = (IFragmentsContainer) context;
            } catch (ClassCastException e) {
                throw new ClassCastException(context.toString() + " must implement ISwitchFragmentListener");
            }
        }
    }

}
