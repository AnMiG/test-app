package com.example.testapp.view.fragments.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.testapp.R;
import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.presenter.contracts.IFavoritesListContract;
import com.example.testapp.presenter.presenters.FavoritesListPresenter;
import com.example.testapp.presenter.view_objects.list.AudioBooksListVO;
import com.example.testapp.presenter.view_objects.list.ListViewObject;
import com.example.testapp.presenter.view_objects.list.MoviesListVO;
import com.example.testapp.presenter.view_objects.list.PodcastsListVO;
import com.example.testapp.view.navigation_strategy.IFragmentsNavigationStrategy;
import com.example.testapp.view.adapters.IFavoriteItemSelectedListener;
import com.example.testapp.view.adapters.favorites.FavoritesAdapter;
import com.example.testapp.view.adapters.favorites.FavoritesParent;
import com.example.testapp.view.fragments.BaseListFragment;
import com.example.testapp.view.navigation_strategy.FragmentsNavigationStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class FavoritesListFragment extends BaseListFragment implements IFavoritesListContract.View, IFavoriteItemSelectedListener {
    @Inject
    protected IFragmentsNavigationStrategy mFragmentsNavigationStrategy;

    public static final String FAVORITES_LIST_KEY = "favorites_list_key";
    private IFavoritesListContract.Presenter mPresenter;
    private List<FavoritesParent> mObjects;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        App.getAppComponent().inject(this);
        if (savedInstanceState != null) {
            this.mObjects = (List<FavoritesParent>) savedInstanceState.getSerializable(FAVORITES_LIST_KEY);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        this.mPresenter = new FavoritesListPresenter(this);
        this.mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        if (this.isObjectsEmpty()) {
            this.mObjects = new ArrayList<>();
            this.mRecyclerView.setAdapter(new FavoritesAdapter(this.getContext(), mObjects, this));
            this.loadList();
        } else {
            this.mRecyclerView.setAdapter(new FavoritesAdapter(this.getContext(), mObjects, this));
            this.mRecyclerView.getAdapter().notifyDataSetChanged();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (!isObjectsEmpty()) {
            outState.putSerializable(FAVORITES_LIST_KEY, new ArrayList<>(mObjects));
        }
    }

    private boolean isObjectsEmpty() {
        return this.mObjects == null || this.mObjects.isEmpty();
    }

    @Override
    public void loadList() {
        this.mPresenter.getList();
    }

    @Override
    public int getTitle() {
        return R.string.title_favourites;
    }

    @Override
    public void showLoading(boolean state) {
        this.mErrorMessageContainer.setVisibility(View.GONE);
        this.mRefreshLayout.setRefreshing(state);
    }

    @Override
    public void showList(List<AudioBooksListVO> audioBooksList, List<MoviesListVO> moviesList, List<PodcastsListVO> podcastssList) {
        super.showList();
        this.mObjects.clear();
        this.mObjects.add(new FavoritesParent(getContext().getString(R.string.title_audio_books), audioBooksList));
        this.mObjects.add(new FavoritesParent(getContext().getString(R.string.title_movies), moviesList));
        this.mObjects.add(new FavoritesParent(getContext().getString(R.string.title_podcasts), podcastssList));
        ((FavoritesAdapter) this.mRecyclerView.getAdapter()).notifyParentDataSetChanged(true);
    }

    @Override
    public void showError(String message) {
        super.showError(message);
    }

    @Override
    public void showAudioBookDetailsFragment(String viewObjectJson) {
        this.mFragmentsNavigationStrategy.showDetailsFragment(FragmentsNavigationStrategy.STATE_FAVORITES, FragmentsNavigationStrategy.STATE_AUDIO_BOOKS, viewObjectJson);
    }

    @Override
    public void showMoviesDetailsFragment(String viewObjectJson) {
        this.mFragmentsNavigationStrategy.showDetailsFragment(FragmentsNavigationStrategy.STATE_FAVORITES, FragmentsNavigationStrategy.STATE_MOVIES, viewObjectJson);
    }

    @Override
    public void showPodcastDetailsFragment(String viewObjectJson) {
        this.mFragmentsNavigationStrategy.showDetailsFragment(FragmentsNavigationStrategy.STATE_FAVORITES, FragmentsNavigationStrategy.STATE_PODCASTS, viewObjectJson);
    }

    @Override
    public void showRemovedFromFavorites() {
        ((FavoritesAdapter) this.mRecyclerView.getAdapter()).notifyParentDataSetChanged(true);
        Toast.makeText(this.getContext(), R.string.message_favorites_removed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        this.showLoading(false);
    }

    @Override
    public int getColorIndex() {
        return FragmentsNavigationStrategy.STATE_FAVORITES;
    }

    @Override
    public void onFavoritesPressed(int parentIndex, int itemIndex) {
        ListViewObject vo = this.mObjects.get(parentIndex).getChildList().get(itemIndex);
        this.mObjects.get(parentIndex).getChildList().remove(vo);
        this.mPresenter.removeFromFavorites(vo);
    }

    @Override
    public void onItemSelected(int parentIndex, int itemIndex) {
        this.mPresenter.onItemSelected(this.mObjects.get(parentIndex).getChildList().get(itemIndex));
    }

    @Override
    public void onStop() {
        super.onStop();
        this.mPresenter.onStop();
    }
}
