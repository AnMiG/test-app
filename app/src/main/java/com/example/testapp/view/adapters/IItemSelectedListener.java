package com.example.testapp.view.adapters;

/**
 * Created by anmig on 02.06.2017.
 */

public interface IItemSelectedListener {
    void onFavoritesPressed(int itemIndex);

    void onItemSelected(int itemIndex);
}