package com.example.testapp.view.activity;

import android.view.MenuItem;

/**
 * Created by anmig on 05.06.2017.
 */

public interface IFragmentsContainer {
    MenuItem[] getMenuItems();
}
