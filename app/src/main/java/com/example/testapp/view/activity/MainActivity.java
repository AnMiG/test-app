package com.example.testapp.view.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.testapp.dependedncy_injection.App;
import com.example.testapp.R;
import com.example.testapp.view.navigation_strategy.IFragmentsNavigationStrategy;
import com.example.testapp.view.fragments.BaseFragment;

import javax.inject.Inject;

import anmig.utils.BottomNavigationViewBehavior;
import anmig.utils.PermissionWrToExStDefinition;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ISwitchFragmentListener, IFragmentsContainer, PermissionWrToExStDefinition.IPermissionCheckListener {
    public static final int[] BOTTOM_BAR_COLORS = new int[]{
            R.color.colorNavigation0,
            R.color.colorNavigation1,
            R.color.colorNavigation2,
            R.color.colorNavigation3
    };
    public static final int[] STATUS_BAR_COLORS = new int[]{
            R.color.colorStatus0,
            R.color.colorStatus1,
            R.color.colorStatus2,
            R.color.colorStatus3
    };
    public static final int[] DETAILS_BACKGROUND_COLORS = new int[]{
            R.color.colorDetails0,
            R.color.colorDetails1,
            R.color.colorDetails2,
            R.color.colorDetails3
    };

    @BindView(R.id.bottom_navigation_view)
    protected BottomNavigationView mNavigationView;

    @BindView(R.id.fragments_container)
    protected FrameLayout mFragmentsContainer;

    @Inject
    protected IFragmentsNavigationStrategy mFragmentsNavigationStrategy;

    private BaseFragment mCurrentFragment;
    private MenuItem[] mMenuItems;
    private CoordinatorLayout.LayoutParams mBottomLayoutParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        App.getAppComponent().inject(this);

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.mBottomLayoutParams = (CoordinatorLayout.LayoutParams) this.mNavigationView.getLayoutParams();
        this.mBottomLayoutParams.setBehavior(new BottomNavigationViewBehavior());

        this.mNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return mFragmentsNavigationStrategy.onNavigationViewPressed(item.getItemId());
            }
        });

        PermissionWrToExStDefinition.checkForPermission(this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        this.mMenuItems = new MenuItem[menu.size()];
        for (int i = 0; i < mMenuItems.length; i++) {
            mMenuItems[i] = menu.getItem(i);
        }
        this.setMenuUnVisible();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.mFragmentsNavigationStrategy.onBackKeyPressed();
                break;
            default:
                this.mCurrentFragment.onOptionsItemSelected(item.getItemId());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void closeApp() {
        this.finish();
    }

    @Override
    public MenuItem[] getMenuItems() {
        return this.mMenuItems;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            this.mFragmentsNavigationStrategy.onBackKeyPressed();
        return false;
    }

    @Override
    public void setNavigationItemSelected(int resId) {
        this.mNavigationView.setSelectedItemId(resId);
    }

    @Override
    public void changeFragment(BaseFragment fragment, boolean changeActionBarColor, boolean changeNavigationBarColor, boolean changeTitle) {
        this.setMenuUnVisible();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.replace(R.id.fragments_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        if (changeActionBarColor) this.changeActionBarColor(fragment.getColorIndex());
        if (changeNavigationBarColor) this.changeNavigationBarColor(fragment.getColorIndex());
        if (changeTitle) this.getSupportActionBar().setTitle(fragment.getTitle());

        ((BottomNavigationViewBehavior) this.mBottomLayoutParams.getBehavior()).slideUp(mNavigationView);

        this.mCurrentFragment = fragment;
    }

    private void setMenuUnVisible() {
        if (mMenuItems == null)
            return;
        for (int i = 0; i < this.mMenuItems.length; i++) {
            this.mMenuItems[i].setVisible(false);
        }
    }


    private void changeNavigationBarColor(int index) {
        int colorCode = BOTTOM_BAR_COLORS[index];
        this.mNavigationView.setBackgroundColor(ContextCompat.getColor(this, colorCode));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            colorCode = STATUS_BAR_COLORS[index];
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, colorCode));
        }
    }

    private void changeActionBarColor(int index) {
        int colorCode = BOTTOM_BAR_COLORS[index];
        this.getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, colorCode)));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            colorCode = STATUS_BAR_COLORS[index];
            getWindow().setStatusBarColor(ContextCompat.getColor(this, colorCode));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionWrToExStDefinition.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onPermissionGranted() {
        this.mFragmentsNavigationStrategy.init(this);
    }

    @Override
    public void onPermissionSuspended() {
        this.mFragmentsNavigationStrategy.init(this);
    }
}
