package com.example.testapp.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.testapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anmig on 02.06.2017.
 */

public abstract class BaseListFragment extends BaseFragment {
    @BindView(R.id.list_recycler_view)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.list_swipe_container)
    protected SwipeRefreshLayout mRefreshLayout;

    @BindView(R.id.error_message_container)
    protected LinearLayout mErrorMessageContainer;

    @BindView(R.id.error_message_text)
    protected TextView mErrorMessageText;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_layout, container, false);
        ButterKnife.bind(this, view);
        mRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        this.mRefreshLayout.setOnRefreshListener(this);
        this.mErrorMessageContainer.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        mImageFetcher.setPauseWork(false);
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    public abstract void loadList();

    public void showList() {
        this.mRecyclerView.setVisibility(View.VISIBLE);
    }

    protected void showError(String message) {
        this.mRecyclerView.setVisibility(View.GONE);
        this.mErrorMessageContainer.setVisibility(View.VISIBLE);
        this.mErrorMessageText.setText(message);
    }

    @Override
    public void onOptionsItemSelected(int itemId) {
        //Do Nothing
    }
}
