package com.example.testapp.view.activity;

import android.view.MenuItem;

import com.example.testapp.view.fragments.BaseFragment;

public interface ISwitchFragmentListener {
    void setNavigationItemSelected(int resId);

    void changeFragment(BaseFragment fragment, boolean changeActionBarColor, boolean changeNavigationBarColor, boolean changeTitle);

    void closeApp();
}
