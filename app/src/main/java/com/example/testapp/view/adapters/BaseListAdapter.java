package com.example.testapp.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.testapp.R;

import java.util.List;

import anmig.utils.cache.ImageFetcher;

/**
 * Created by anmig on 02.06.2017.
 */

public abstract class BaseListAdapter<T> extends RecyclerView.Adapter<ViewHolder> {
    protected List<T> mObjects;
    protected Context mContext;
    protected IItemSelectedListener mItemSelectedListener;
    protected ImageFetcher mImageFetcher;

    public BaseListAdapter(Context pContext, List<T> pDataObjects, ImageFetcher pImageFetcher, IItemSelectedListener pItemSelectedListener) {
        this.mObjects = pDataObjects;
        this.mContext = pContext;
        this.mImageFetcher = pImageFetcher;
        this.mItemSelectedListener = pItemSelectedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(v, this.mContext, mImageFetcher, this.mItemSelectedListener);
    }

    @Override
    public int getItemCount() {
        if (this.mObjects == null)
            return 0;
        return this.mObjects.size();
    }
}