package com.example.testapp.view.navigation_strategy;

/**
 * Created by anmig on 11.06.2017.
 */

public class AppState {
    public enum StateType {
        LIST, DETAILS;
    }

    private int mIndex;
    private String mDetailsObjectBundle;
    private StateType mType;
    private int mDetailsIndex;

    AppState(int pIndex) {
        this.mIndex = pIndex;
        this.mType = StateType.LIST;
    }

    AppState(int pIndex, String pDetailsObjectBundle) {
        this.mIndex = pIndex;
        this.mDetailsObjectBundle = pDetailsObjectBundle;
        this.mType = StateType.DETAILS;
    }

    public int getIndex() {
        return mIndex;
    }

    public int getDetailsIndex() {
        return mDetailsIndex;
    }

    public void setDetailsIndex(int mDetailsIndex) {
        this.mDetailsIndex = mDetailsIndex;
    }

    public StateType getType() {
        return mType;
    }

    public void setType(StateType pType) {
        this.mType = pType;
    }

    public String getDetailsObjectBundle() {
        return mDetailsObjectBundle;
    }

    public void setDetailsObjectBundle(String mDetailsObjectBundle) {
        this.mDetailsObjectBundle = mDetailsObjectBundle;
    }
}
