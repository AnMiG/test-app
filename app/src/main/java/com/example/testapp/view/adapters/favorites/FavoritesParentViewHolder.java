package com.example.testapp.view.adapters.favorites;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.example.testapp.R;

/**
 * Created by anmig on 05.06.2017.
 */

public class FavoritesParentViewHolder extends ParentViewHolder {
    private TextView mText;
    private RelativeLayout mBackground;

    public FavoritesParentViewHolder(View itemView) {
        super(itemView);
        this.mText = (TextView) itemView.findViewById(R.id.list_header_title);
        this.mBackground = (RelativeLayout) itemView.findViewById(R.id.list_header_background);
    }

    public void setHeaderText(String pText) {
        this.mText.setText(pText);
    }

    public void setHeaderBacgroune(int res) {
        this.mBackground.setBackgroundResource(res);
    }
}
