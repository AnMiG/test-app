package anmig.utils;

import android.util.Log;

import com.example.testapp.BuildConfig;

/**
 * Created by anmig on 04.06.2017.
 */

public class T {
    private static StringBuilder str = new StringBuilder();
    public static final String TAG = "AnMiG";

    public static void i(Object... messages) {
        if (BuildConfig.DEBUG) {
            str.setLength(0);
            for (int i = 0; i < messages.length; i++) {
                str.append(messages[i]);
            }
            Log.d(TAG, str.toString());
        }
    }

    public static void e(Object message) {
        throw new Error("" + message);
    }
}
