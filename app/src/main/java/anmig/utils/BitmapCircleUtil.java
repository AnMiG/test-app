package anmig.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.TypedValue;

import anmig.utils.cache.RecyclingBitmapDrawable;
import anmig.utils.cache.Utils;


public class BitmapCircleUtil {
    public static Bitmap createCircleBitmap(Bitmap bitmap) {
        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }

    public static Bitmap createCircleBitmapWithBorder(Bitmap bitmap, float strokeWidth, int pColor) {
        Bitmap output;
        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight() + 8, bitmap.getHeight() + 8, Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth() + 8, bitmap.getWidth() + 8, Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r + 4, r + 4, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        paint.setXfermode(null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(pColor);
        paint.setStrokeWidth(strokeWidth);
        canvas.drawCircle(r + 4, r + 4, r, paint);

//        int w = bitmap.getWidth();
//        int h = bitmap.getHeight();
//
//        int radius = Math.min(h / 2, w / 2);
//        Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Bitmap.Config.ARGB_8888);
//
//        Paint p = new Paint();
//        p.setAntiAlias(true);
//
//        Canvas c = new Canvas(output);
//        c.drawARGB(0, 0, 0, 0);
//        p.setStyle(Paint.Style.FILL);
//
//        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);
//
//        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//
//        c.drawBitmap(bitmap, 4, 4, p);
//        p.setXfermode(null);
//        p.setStyle(Paint.Style.STROKE);
//        p.setColor(pColor);
//        p.setStrokeWidth(strokeWidth);
//        c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);

        return output;
    }

    public static BitmapDrawable createCircleDrawable(Context pContext, Bitmap pBitmap) {
        Bitmap bitmap = createCircleBitmap(pBitmap);
        if (Utils.hasHoneycomb()) {
            return new BitmapDrawable(pContext.getResources(), bitmap);
        } else {
            return new RecyclingBitmapDrawable(pContext.getResources(), bitmap);
        }
    }

    public static BitmapDrawable createCircleDrawableWithBorder(Context pContext, Bitmap pBitmap, float strokeWidth, int pColor) {
        Bitmap bitmap = createCircleBitmapWithBorder(pBitmap, strokeWidth, pColor);
        if (Utils.hasHoneycomb()) {
            return new BitmapDrawable(pContext.getResources(), bitmap);
        } else {
            return new RecyclingBitmapDrawable(pContext.getResources(), bitmap);
        }
    }
}
