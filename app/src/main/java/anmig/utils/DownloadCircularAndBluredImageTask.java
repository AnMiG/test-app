package anmig.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.TypedValue;
import android.widget.ImageView;

import com.example.testapp.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by anmig on 04.06.2017.
 */

public class DownloadCircularAndBluredImageTask extends AsyncTask<DownloadCircularAndBluredImageTask.Params, Void, Drawable[]> {
    public static class Params {
        private String url;
        private int imageWidth;
        private int imageHeight;

        public Params(String url, int imageWidth, int imageHeight) {
            this.url = url;
            this.imageWidth = imageWidth;
            this.imageHeight = imageHeight;
        }

        public String getUrl() {
            return url;
        }

        public int getImageWidth() {
            return imageWidth;
        }

        public int getImageHeight() {
            return imageHeight;
        }
    }

    private Context mContext;
    private ImageView mCircularImageView;
    private ImageView mBackgroundImageView;
    private int mBackgroundColorResourceIfNotLoaded;
    private int mCircularResourceIfNotLoaded;

    public DownloadCircularAndBluredImageTask(Context pContext, ImageView pCircularImageView, ImageView pBackgroundImageView, int pCircularResourceIfNotLoaded, int pBackgroundColorResourceIfNotLoaded) {
        this.mContext = pContext;
        this.mCircularImageView = pCircularImageView;
        this.mBackgroundImageView = pBackgroundImageView;
        this.mBackgroundColorResourceIfNotLoaded = pBackgroundColorResourceIfNotLoaded;
        this.mCircularResourceIfNotLoaded = pCircularResourceIfNotLoaded;
    }

    protected Drawable[] doInBackground(DownloadCircularAndBluredImageTask.Params... pParams) {
        String urldisplay = pParams[0].getUrl();
        Drawable[] results = null;
        Bitmap bitmap = null;
        InputStream in = null;
        try {
            in = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
            int minBitmapSize = Math.min(bitmap.getWidth(), bitmap.getHeight());
            int minImageSize = Math.min(pParams[0].getImageWidth(), pParams[0].getImageHeight());
            if (minBitmapSize < minImageSize) {
                float ratio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
                bitmap = Bitmap.createScaledBitmap(bitmap, pParams[0].getImageWidth(), (int) ((float) pParams[0].getImageWidth() / ratio), false);
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                if (in != null) in.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        results = new Drawable[2];
        if (bitmap != null) {
            results[1] = BitmapBlurUtil.createBluredDrawable(this.mContext, bitmap, 5f, 0.8f);
        } else {
            bitmap = BitmapFactory.decodeResource(mContext.getResources(), mCircularResourceIfNotLoaded);
            results[1] = new ColorDrawable(this.mBackgroundColorResourceIfNotLoaded);
        }
        results[0] = BitmapCircleUtil.createCircleDrawableWithBorder(this.mContext, bitmap, 3, Color.WHITE);
        return results;
    }

    protected void onPostExecute(Drawable[] results) {
        if (results != null) {
            mCircularImageView.setImageDrawable(results[0]);
            mBackgroundImageView.setImageDrawable(results[1]);
        }
    }
}