package anmig.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.TypedValue;


/**
 * Add this to build.gradle defaultConfig
 * renderscriptTargetApi 18
 * renderscriptSupportModeEnabled true (can not be used in one project with support:appcompat-v7 library )
 * <p>
 * if renderscriptSupportModeEnabled = true, use
 * android.renderscript.v8.RenderScript instead of  android.renderscript.v8.RenderScript
 * <p>
 * if false - the support library of renderscript will not work, so you have to use package:
 * android.renderscript.v8.RenderScript  and also
 * need to check  if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
 * ....
 * }
 */

public class BitmapBlurUtil {

    /**
     * Add this to build.gradle defaultConfig
     * renderscriptTargetApi 19
     * renderscriptSupportModeEnabled true
     *
     * @param pContext
     * @param image
     * @return
     */
    public static Bitmap createBluredBitmap(Context pContext, Bitmap image, float bitmapScale, float blurRadius) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            int width = Math.round(image.getWidth() * bitmapScale);
            int height = Math.round(image.getHeight() * bitmapScale);

            Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
            Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

            RenderScript rs = RenderScript.create(pContext);
            ScriptIntrinsicBlur theIntrinsic = null;
            theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
            Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
            Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
            theIntrinsic.setRadius(blurRadius);
            theIntrinsic.setInput(tmpIn);
            theIntrinsic.forEach(tmpOut);
            tmpOut.copyTo(outputBitmap);
            return outputBitmap;
        }
        return image;
    }

    public static BitmapDrawable createBluredDrawable(Context pContext, Bitmap bitmap, float pBlurRadius, float pBlurScale) {
        float blurRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, pBlurRadius, pContext.getResources().getDisplayMetrics());
        Bitmap blurredBitmap = BitmapBlurUtil.createBluredBitmap(pContext, bitmap, pBlurScale, blurRadius);
        BitmapDrawable dr = new BitmapDrawable(pContext.getResources(), blurredBitmap);
        return dr;
    }
}
