package anmig.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.TypedValue;
import android.widget.ImageView;

import com.example.testapp.R;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by anmig on 04.06.2017.
 */

public class DownloadCircularImageTask extends AsyncTask<String, Void, Drawable> {
    private Context mContext;
    private ImageView mImageView;
    private int mReservedResourceIfNotLoaded;

    public DownloadCircularImageTask(Context pContext, ImageView pImageView, int pReservedResourceIfNotLoaded) {
        this.mContext = pContext;
        this.mImageView = pImageView;
        this.mReservedResourceIfNotLoaded = pReservedResourceIfNotLoaded;
    }

    protected Drawable doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap bitmap = null;
        InputStream in = null;
        try {
            in = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
            in.close();
        } catch (Exception e) {
            try {
                if (in != null) in.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        if (bitmap == null)
            bitmap = BitmapFactory.decodeResource(mContext.getResources(), mReservedResourceIfNotLoaded);
        return createRoundedDrawable(bitmap);
    }

    private Drawable createRoundedDrawable(Bitmap bitmap) {
        Resources r = this.mContext.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, r.getDimension(R.dimen.list_item_image_size), r.getDisplayMetrics());
        bitmap = Bitmap.createScaledBitmap(bitmap, (int) px, (int) px, false);
        RoundedBitmapDrawable dr = RoundedBitmapDrawableFactory.create(mContext.getResources(), bitmap);
        dr.setAntiAlias(true);
        dr.setCircular(true);
        return dr;
    }

    protected void onPostExecute(Drawable result) {
        if (result != null)
            mImageView.setImageDrawable(result);
        //ContextCompat.getDrawable(this.mContext, R.drawable.ic_star_border_white_24dp)
    }
}
