package anmig.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class PermissionWrToExStDefinition {
    public interface IPermissionCheckListener {
        void onPermissionGranted();

        void onPermissionSuspended();
    }

    private static final int REQUEST_WRITE_STORAGE = 112;
    private static IPermissionCheckListener permissionCheckListener;

    public static void checkForPermission(Activity pActivity, IPermissionCheckListener pPermissionCheckListener) {
        permissionCheckListener = pPermissionCheckListener;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(pActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(pActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
                return;
            }
        }
        permissionCheckListener.onPermissionGranted();
        permissionCheckListener = null;
    }

    public static void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //Add this to main activity
        //@Override
        //public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //    PermissionWrToExStDefinition.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //}
        if (requestCode == REQUEST_WRITE_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (permissionCheckListener != null) {
                    permissionCheckListener.onPermissionGranted();
                    permissionCheckListener = null;
                    return;
                }
            } else {
                if (permissionCheckListener != null) {
                    permissionCheckListener.onPermissionSuspended();
                    permissionCheckListener = null;
                    return;
                }
            }
        }
    }
}